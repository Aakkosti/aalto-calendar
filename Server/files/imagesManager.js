var EventEmitter = require('events').EventEmitter;
var imagesManager = new EventEmitter();
var fs = require('fs');
var urlLib = require('url');
var usersManager = require('./usersManager');

////////////////////////////////////////////////////
// Public
///////////////////////////////////////////////////

imagesManager.processImagesRequest = function(request, response) {
	var _callback = function(userId) {
		if (!userId) {
			return;
		}
		var method = request.method;
		if (method === 'POST') {
			console.log('post');
			var data = request.body;
			var timestamp = new Date().getTime();
			fs.writeFile('./images/' + timestamp.toString(), data, 'binary', function(error) {
				if (error) {
					var result = {
						'error': 'couldn\' save the image'
					};
					imagesManager.emit('error', 500, result, response);
				}
				else {
					var result = {
						'success': 'the image was successfully saved',
						'url': 'images/' + timestamp.toString()
					};
					var headers = {'Content-Type': 'application/json'};
					imagesManager.emit('data', result, headers, response);
				}
			});
		}
		else{
			imagesManager.emit('error', 405, null, response);
		}
	};
	usersManager.authenticate(request, response, _callback);
};

imagesManager.processImageRequest = function(request, response) {
	var method = request.method;
	if (method === 'GET') {
		var regex = new RegExp('^/images/([0-9]+)/?$', 'i');
		var url = request.url;
		var path = urlLib.parse(url).pathname;
		var regexResult = regex.exec(path);
		var fileName = regexResult[1];
		console.log(fileName);
		fs.readFile('./images/' + fileName, function (error, data) {
			if (error) {
				var result = {
					'error': 'image doesn\'t exist'	
				};
				imagesManager.emit('error', 500, result, response);
			}
			else {
				var headers = {'Content-Type': 'image/png'};
				imagesManager.emit('data', data, headers, response);
			}
		});
	}
	else {
		imagesManager.emit('error', 405, null, response);
	}
};

module.exports = imagesManager;