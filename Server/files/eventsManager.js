var pgManager = require('./pgManager');
var usersManager = require('./usersManager');
var EventEmitter = require('events').EventEmitter;
var eventsManager = new EventEmitter();
var urlLib = require('url');

var months = {
	"Jan": '01',
	"Feb": '02',
	"Mar": '03',
	"Apr": '04',
	"May": '05',
	"Jun": '06',
	"Jul": '07',
	"Aug": '08',
	"Sep": '09',
	"Oct": '10',
	"Nov": '11',
	"Dec": '12'
};

///////////////////////////////////////////////////////////////////////
// Private 
///////////////////////////////////////////////////////////////////////

function _getStandardDate(date) {
	var regex = new RegExp('[A-Z][a-z]{2} ([A-Z][a-z]{2}) ([0-9]{2}) ([0-9]{4}) ([0-9]{2}:[0-9]{2}:[0-9]{2}) GMT(\\+|-)([0-9]{2}).*');
	var regexArray = regex.exec(date);
	var month = months[regexArray[1]];
	var day = regexArray[2];
	var year = regexArray[3];
	var time = regexArray[4];
	var sign = regexArray[5];
	var timezone = regexArray[6];
	var result = year + '-' + month + '-' + day + ' ' + time + ' ' + sign + timezone + ':00';
	return result;
}

function _emitEvent(statusCode, result, response) {
	if (statusCode) {
		eventsManager.emit('error', statusCode, result, response);
	} 

	else {
		eventsManager.emit('data', result, response);
	}
}

function _getEvent(id, response) {
	var query = 'SELECT event.id, event.title, event.date, event.description, event.location, event.address, event.image_url, event.image_identifier, event.application_user, application_user.username AS owner_username FROM event LEFT OUTER JOIN application_user ON (event.application_user = application_user.id) WHERE event.id = $1';
	var parameters = [id];
	var _callback = function(statusCode, result) {
		if (statusCode) {
			result = {
				'error': 'couldnt\'t fetch event'
			};
		}
		else {
			if (result.length != 0) {
				var date = result[0].date;
				result[0].date = _getStandardDate(date);
			}
		}
		_emitEvent(statusCode, result[0], response);
	};
	pgManager.processQuery(query, parameters, _callback);
}

function _getEvents(response) {
	var query = 'SELECT event.id, event.title, event.date, event.description, event.location, event.address, event.image_url, event.image_identifier, event.application_user, application_user.username AS owner_username FROM event LEFT OUTER JOIN application_user ON (event.application_user = application_user.id)';
	var parameters = [];
		var _callback = function(statusCode, result) {
			if (statusCode) {
				result = {
					'error': 'couldnt\'t fetch events'
				};
			}
			else {
				for (var key in result) {
					var date = result[key].date;
					result[key].date = _getStandardDate(result[key].date);
				}
			}
			_emitEvent(statusCode, result, response);
	};
	pgManager.processQuery(query, parameters, _callback);
}

function _deleteEvent(id, userId, response) {
	var selectQuery = "SELECT application_user FROM event WHERE id = $1";
	var _selectCallback = function(statusCode, result) {
		errorResult = {
			'error': 'Couldn\'t delete event ' + id
		};

		if (statusCode) {
			result = errorResult;
			_emitEvent(statusCode, result, response);
		}

		else if (result.length === 0) {
			result = errorResult;
			statusCode = 400;
			_emitEvent(statusCode, result, response);

		}

		else if (result[0].application_user !== userId) {
			result = errorResult;
			statusCode = 403;
			_emitEvent(statusCode, result, response);
		}

		else {
			var _updateCallback = function(statusCode, result) {
				if (statusCode) {
					result = errorResult;
					_emitEvent(statusCode, result, response);
				}
				else {
					result = {
						'success': 'event ' + id + ' was deleted'
					};
					_emitEvent(null, result, response);
				}
			};
			var deleteQuery = "DELETE FROM event WHERE id = $1";
			pgManager.processQuery(deleteQuery, [id], _updateCallback);
		}
	}
	pgManager.processQuery(selectQuery, [id], _selectCallback);
}

function _parseJSON(text) {
	try {
		var json = JSON.parse(text);
	} catch (e) {
		return null;
	}
	return json;
}

function _addEvent(userId, body, response) {
	var json = _parseJSON(body);
	if (json) {
		var parameters = [
		json.title,
		json.date,
		json.description,
		json.location,
		json.address,
		json.image_url,
		json.image_identifier,
		userId];
		for (var parameter in parameters) {
			if (parameter != 5 && parameter != 6 && !parameters[parameter]) {
				var statusCode = 400;
				var result = {
					'error': 'JSON invalid'
				};
				_emitEvent(400, result, response);
				return;
			}
		}

		var query = "INSERT INTO event (title, date, description, location, address, image_url, image_identifier, application_user) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id;";
		var _callback = function(statusCode, result) {
			if (statusCode) {
				result = {
					'error': 'couldn\' add event'
				};
			} else {
				result = {
					'success': 'event ' + result[0].id + ' was added',
					'id': result[0].id
				};
			}
			_emitEvent(statusCode, result, response);
		};
		pgManager.processQuery(query, parameters, _callback);
	}

	else {
		var statusCode = 400;
		var result = {
			'error': 'JSON invalid'
		};
		_emitEvent(400, result, response);
	}
}

function _putEvent(id, userId, body, response) {
	var json = _parseJSON(body);
	if (json) {
		var parameters = [
		id,
		json.title,
		json.date,
		json.description,
		json.location,
		json.address,
		json.image_url,
		json.image_identifier];

		for (var parameter in parameters) {
			if (parameter != 6 && parameter != 7 && !parameters[parameter]) {
				var statusCode = 400;
				var result = {
					'error': 'JSON invalid'
				};
				_emitEvent(400, result, response);
				return;
			}
		}

		parameters.push(userId);

		var selectQuery = "SELECT application_user FROM event WHERE id = $1";
		var updateQuery = "UPDATE event SET title = $2, date = $3, description = $4, location = $5, address = $6, image_url = $7, image_identifier = $8 WHERE id = $1 AND application_user = $9";

		var _selectCallback = function(statusCode, result) {
			errorResult = {
				'error': 'Couldn\'t update event ' + id
			};

			if (statusCode) {
				result = errorResult;
				_emitEvent(statusCode, result, response);
			}

			else if (result.length === 0) {
				result = errorResult;
				statusCode = 400;
				_emitEvent(statusCode, result, response);

			}

			else if (result[0].application_user !== userId) {
				result = errorResult;
				statusCode = 403;
				_emitEvent(statusCode, result, response);
			}

			else {
				var _updateCallback = function(statusCode, result) {
					if (statusCode) {
						result = errorResult;
						_emitEvent(statusCode, result, response);
					}
					else {
						result = {
						'success': 'event ' + id + ' was updated'
						};
						_emitEvent(null, result, response);
					}
				};
				pgManager.processQuery(updateQuery, parameters, _updateCallback);
			}
		};
		pgManager.processQuery(selectQuery, [id], _selectCallback);

	} else {
		var statusCode = 400;
		var result = {
			'error': 'JSON invalid'
		};
		_emitEvent(statusCode, result, response);
	} 
}

///////////////////////////////////////////////////////////////////////
// Public 
///////////////////////////////////////////////////////////////////////

eventsManager.processEventsRequest = function(request, response) {
	var _callback = function(userId) {
		if (!userId) {
			return;
		}
		var method = request.method;
		if (method === 'GET') {
			_getEvents(response);
		}

		else if (method === 'POST') {
			_addEvent(userId, request.body, response);
		}

		else {
			_emitEvent(405, null, response);
		}
	};
	usersManager.authenticate(request, response, _callback);
};

eventsManager.processEventRequest = function(request, response) {
	var _callback = function(userId) {
		if (!userId) {
			return;
		}

		var method = request.method;
		var regex = new RegExp('^/events/([0-9]+)/?$', 'i');
		var url = request.url;
		var path = urlLib.parse(url).pathname;
		var regexResult = regex.exec(path);
		var id = regexResult[1];

		if (method === 'GET') {
			_getEvent(id, response);
		} 

		else if (method === 'PUT') {
			_putEvent(id, userId, request.body, response);
		} 

		else if (method === "DELETE") {
			_deleteEvent(id, userId, response);
		}

		else {
			_emitEvent(405, null, response);
		}
	};
	usersManager.authenticate(request, response, _callback);
};

module.exports = eventsManager;
