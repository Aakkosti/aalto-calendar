var requestManager = require('./requestManager');
var urlLib = require('url');

///////////////////////////////////////////////////////////////////////
// Public 
///////////////////////////////////////////////////////////////////////

exports.dispatch = function(request, response) {
	var patterns = {
		events: new RegExp('^/events/?$', 'i'),
		event: new RegExp('^/events/[0-9]+/?$', 'i'),
		user: new RegExp('^/user/?$', 'i'),
		images: new RegExp('^/images/?$', 'i'),
		image: new RegExp('^/images/[0-9]+/?$', 'i')
	};
	var methodsName = {
		events: 'processEventsRequest',
		event: 'processEventRequest',
		user: 'processUserRequest',
		images: 'processImagesRequest',
		image: 'processImageRequest'
	};
	var url = request.url;
	var path = urlLib.parse(url).pathname;
	for (var patternKey in patterns) {
		if (path.match(patterns[patternKey])) {
			var methodName = methodsName[patternKey];
			requestManager[methodName](request, response);
			return;
		}
	}
	response.writeHeader(404);
	response.end();
};
