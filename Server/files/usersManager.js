var pgManager = require('./pgManager');
var EventEmitter = require('events').EventEmitter;
var usersManager = new EventEmitter();
var crypto = require('crypto');

///////////////////////////////////////////////////////////////////////
// Private 
///////////////////////////////////////////////////////////////////////

function _emitEvent(statusCode, result, headers, response) {
	if (statusCode) {
		usersManager.emit('error', statusCode, result, headers, response);
	} 

	else {
		usersManager.emit('data', result, response);
	}
}

function _parseJSON(text) {
	try {
		var json = JSON.parse(text);
	} catch (e) {
		return null;
	}
	return json;
}

function _putUser(body, response) {
	var json = _parseJSON(body);
	if (json) {
		var insertParameters = [
			json.username,
			json.password
		];

		for (var key in insertParameters) {
			if (!insertParameters[key]) {
				var statusCode = 400;
				var result = {
					'error': 'JSON invalid'
				};
				_emitEvent(400, result, null, response);
				return;
			}
		}

		var hash = crypto.createHash('sha1');
		hash.update(insertParameters[1]);
		insertParameters[1] = hash.digest('hex');

		var selectParameters = [insertParameters[0]];

		var selectQuery = "SELECT id FROM application_user WHERE username = $1";
		var _selectCallback = function(statusCode, result) {
			if (statusCode) {
				result = {
					'error': 'couldnt\'t create user'
				};
				_emitEvent(statusCode, result, null, response);
				return;
			}
			if (result.length != 0) {
				result = {
					'error': 'username already in use'
				};
				_emitEvent(400, result, null, response);
				return;
			}
			var _insertCallback = function(statusCode, result) {
				if (statusCode) {
					result = {
						'error': 'couldn\'t create user'
					};
					_emitEvent(statusCode, result, null, response);
					return;
				}
				result = {
					'success': 'user created',
					'username': result[0].username,
					'id': result[0].id
				};
				_emitEvent(null, result, null, response);
			};
			var insertQuery = "INSERT INTO application_user (username, password) VALUES ($1, $2) RETURNING id, username";
			pgManager.processQuery(insertQuery, insertParameters, _insertCallback);
		};
		pgManager.processQuery(selectQuery, selectParameters, _selectCallback);
	}

	else {
		var statusCode = 400;
		var result = {
			'error': 'JSON invalid'
		};
		_emitEvent(400, result, null, response);
	}
}

///////////////////////////////////////////////////////////////////////
// Public 
///////////////////////////////////////////////////////////////////////

usersManager.authenticate = function (request, response, callback) {
	var headers = request.headers;
	var authenticationHeader = headers.authorization;
	function _sendForbiddenResponse() {
		result = {
			'error': 'incorrect username/password'
		};
		_emitEvent(403, result, null, response);
		callback(null);
	};
	if (!authenticationHeader) {
		var replyHeaders = {'WWW-Authenticate': 'Basic realm="aalto-calendar"'};
		_emitEvent(401, result, replyHeaders, response);
		callback(null);
		return;
	}
	var regex = new RegExp('^Basic (.*)$');
	var result = regex.exec(authenticationHeader);
	if (!result || result.length !== 2) {
		_sendForbiddenResponse();
		return;
	}
	var usernameAndPass = new Buffer(result[1], 'base64').toString('ascii');
	regex = new RegExp('^(.*):(.*)$');
	result = regex.exec(usernameAndPass);
	if (!result || result.length !== 3) {
		_sendForbiddenResponse();
		return;
	}
	var username = result[1];
	var password = result[2];
	var query = "SELECT id, password FROM application_user WHERE username = $1";
	var parameters = [username];
	var _callback = function(statusCode, result) {
		if (statusCode) {
			result = {
				'error': 'couldn\'t verify the user'
			};
			_emitEvent(statusCode, result, null, response);
			callback(null);
			return;
		}

		if (!result[0]) {
			_sendForbiddenResponse();
			return;
		}

		var hash = crypto.createHash('sha1');
		hash.update(password);
		var hashedPassword = hash.digest('hex');
		if (hashedPassword === result[0].password) {
			callback(result[0].id);
		}
		else {
			callback(null);
		}
	};
	pgManager.processQuery(query, parameters, _callback);
}

usersManager.processUserRequest = function(request, response) {
	var method = request.method;
	if (method === 'GET') {
		var _callback = function(userId) {
			if (userId) {
				var result = {
					'id': userId,
					'success': 'username and password match'
				};
				_emitEvent(null, result, null, response);
			}
			else {
				
			}
		};
		this.authenticate(request, response, _callback);
	}

	else if (method === 'PUT') {
		var body = request.body;
		_putUser(body, response);
	}

	else {
		_emitEvent(405, null, null, response);
	}
};

module.exports = usersManager;