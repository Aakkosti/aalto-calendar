var pg = require('pg');

///////////////////////////////////////////////////////////////////////
// Public 
///////////////////////////////////////////////////////////////////////

exports.processQuery = function(query, parameters, callback) {
	var callback = callback || function(){};
	pg.connect(process.env.DATABASE_URL, function(error, client, done) {
		if (error) {
			done();
			callback(500, null);
			return;
		}
		client.query(query, parameters, function(error, result) {
			if (error) {
				callback(500, null);
				done();
				return;
			}
			callback(null, result.rows);
			done();
		});
	});
};