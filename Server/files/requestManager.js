var eventsManager = require('./eventsManager');
var usersManager = require('./usersManager');
var imagesManager = require('./imagesManager');

///////////////////////////////////////////////////////////////////////
// Private 
///////////////////////////////////////////////////////////////////////

eventsManager.on('error', function(statusCode, result, response) {
	var headers = {'Content-Type': 'application/json'};
	_reply(statusCode, response, headers, result);
});

eventsManager.on('data', function(result, response) {
	var headers = {'Content-Type': 'application/json'};
	_reply(200, response, headers, result);
});

usersManager.on('error', function(statusCode, result, headers, response) {
	if (headers) {
		headers['Content-Type'] = 'application/json';
	} else {
		var headers = {'Content-Type': 'application/json'};
	}
	_reply(statusCode, response, headers, result);
});

usersManager.on('data', function(result, response) {
	var headers = {'Content-Type': 'application/json'};
	_reply(200, response, headers, result);
});

imagesManager.on('error', function(statusCode, result, response) {
	var headers = {'Content-Type': 'application/json'};
	_reply(statusCode, response, headers, result);
});

imagesManager.on('data', function(result, headers, response) {
	_reply(200, response, headers, result);
});

function _reply(statusCode, response, headers, body) {
	response.writeHeader(statusCode, headers);
	if (body) {
		if (headers['Content-Type'] === 'application/json') {
			var json = JSON.stringify(body);
			response.write(json);
		}
		else {
			response.write(body);
		}
	}
	response.end();
}

///////////////////////////////////////////////////////////////////////
// Public 
///////////////////////////////////////////////////////////////////////

exports.processEventRequest = function(request, response) {
	eventsManager.processEventRequest(request, response);
};

exports.processEventsRequest = function(request, response) {
	eventsManager.processEventsRequest(request, response);
};

exports.processUserRequest = function(request, response) {
	usersManager.processUserRequest(request, response);
};

exports.processImagesRequest = function(request, response) {
	imagesManager.processImagesRequest(request, response);
};

exports.processImageRequest = function(request, response) {
	imagesManager.processImageRequest(request, response);
};