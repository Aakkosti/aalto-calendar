var http = require('http');
var requestsManager = require('./files/requestsManager');
var fs = require('fs');

fs.readFile('./testForm.html', function (err, data) {
   	if (err) {
       	throw err;
    }
    index = data;
});

var directories = fs.readdirSync('./');
if (directories.indexOf('images') === - 1) {
	fs.mkdirSync('./images');
}

var port = process.env.PORT || 5000;

http.createServer(function(request, response) {
	if (request.url === '/testForm') {
		response.writeHeader(200, {"Content-Type": "text/html"});
    	response.write(index);
    	response.end();
	}

	var method = request.method;
	if (method === 'POST' || method === 'PUT') {
		var body = [];
		if (request.url === '/images/' || request.url === '/images') {
			body = '';
			request.setEncoding('binary');
		};
		request.on('data', function(chunk) {
			if (request.url === '/images/' || request.url === '/images') {
				body += chunk;
			}
			else 
				body.push(chunk);
		});

		request.on('end', function() {
			request.body = body;
			requestsManager.dispatch(request, response);
		});

		request.on('close', function() {
		});

	} else {
		requestsManager.dispatch(request, response);
	}
}).listen(port);