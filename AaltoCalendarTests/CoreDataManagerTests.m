//
//  CoreDataManagerTests.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 24/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "CoreDataManagerTests.h"
#import "CoreDataManager.h"
#import "Event.h"

@implementation CoreDataManagerTests

- (void)testSaveInBackgroundSynchronously {
	NSFileManager* fileManager = [NSFileManager defaultManager];
	NSURL* documentURL = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
	NSURL *storeURL = [documentURL URLByAppendingPathComponent:@"AaltoCalendar.sqlite"];
	[fileManager removeItemAtURL:storeURL error:nil];
	
	CoreDataManager* manager = [CoreDataManager sharedManager];
	Event* event = [NSEntityDescription insertNewObjectForEntityForName:@"Event"
												 inManagedObjectContext:manager.moc];
	event.address = @"my address";
	event.date = [NSDate date];
	event.event_description = @"my description";
	event.identifier = [NSNumber numberWithInt:1];
	event.image_url = @"my image url";
	event.location = @"my location";
	event.title = @"my title";
	
	[manager saveToDiskAsynchronously:NO];

	NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:@"Event"];
	NSError* error = nil;
	NSArray* array = [manager.moc executeFetchRequest:request
												error:&error];
	STAssertNil(error, @"error during save");
	STAssertEquals(array.count, 1U, @"more than one object returned");
	STAssertEqualObjects([array objectAtIndex:0], event, @"object returned different than the one saved");
}
@end
