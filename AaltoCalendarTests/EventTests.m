//
//  EventTests.m
//  AaltoCalendar
//
//  Created by Jaakko Kaisanlahti on 2013/03/23.
//  Copyright (c) 2013年 Group 14. All rights reserved.
//

#import "EventTests.h"
#import "Event+Util.h"
#import "Config.h"

@implementation EventTests

- (NSManagedObjectContext*)managedObjectContextWithConcurrencyType:(NSManagedObjectContextConcurrencyType)concurrencyType {
    NSManagedObjectModel *mom = [NSManagedObjectModel mergedModelFromBundles:nil];
    STAssertNotNil(mom, @"Can not create MOM from main bundle");
    
    NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    STAssertNotNil(psc, @"Can not create persistent store coordinator");
    NSPersistentStore *store = [psc addPersistentStoreWithType:NSInMemoryStoreType configuration:nil URL:nil options:nil error:0];
    STAssertNotNil(store, @"Can not create In-Memory persistent store");
    
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:concurrencyType];
    moc.persistentStoreCoordinator = psc;
    
    return moc;
}
- (void)testeventWithDictionary {
    NSManagedObjectContext *context = [self managedObjectContextWithConcurrencyType:NSMainQueueConcurrencyType];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    STAssertNil([Event eventWithDictionary:dictionary fromContext:context], @"Response \
                not nil for an incomplete dictionary.");
    NSNumber *idNumber = [NSNumber numberWithInt:1];
    [dictionary setObject:idNumber forKey:EVENT_ID];
    STAssertNil([Event eventWithDictionary:dictionary fromContext:context], @"Response \
                not nil for an incomplete dictionary.");
    NSString *title = @"test party";
    [dictionary setObject:title forKey:EVENT_TITLE];
    STAssertNil([Event eventWithDictionary:dictionary fromContext:context], @"Response \
                not nil for an incomplete dictionary.");
    NSString *description = @"description";
    [dictionary setObject:description forKey:EVENT_DESCRIPTION];
    STAssertNil([Event eventWithDictionary:dictionary fromContext:context], @"Response \
                not nil for an incomplete dictionary.");
    NSString *date = @"2003-02-04 15:30:00 +02:00";
    [dictionary setObject:date forKey:EVENT_DATE];
    STAssertNil([Event eventWithDictionary:dictionary fromContext:context], @"Response \
                not nil for an incomplete dictionary.");
    NSString *location = @"location";
    [dictionary setObject:location forKey:EVENT_LOCATION];
    STAssertNil([Event eventWithDictionary:dictionary fromContext:context], @"Response \
                not nil for an incomplete dictionary.");
    NSString *address = @"address";
    [dictionary setObject:address forKey:EVENT_ADDRESS];
    STAssertNil([Event eventWithDictionary:dictionary fromContext:context], @"Response \
                not nil for an incomplete dictionary.");
    NSString *image_url = @"url";
    [dictionary setObject:image_url forKey:EVENT_IMAGE_URL];
    STAssertNotNil([Event eventWithDictionary:dictionary fromContext:context], @"Response \
                nil for a complete dictionary.");
    
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:
                                                         @"Event"
                                                         inManagedObjectContext:
                                                            context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"identifier == %@",
                               [dictionary objectForKey:@"id"]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"%@", error);
    }
    Event *event = [array objectAtIndex:0];
    STAssertEqualObjects([event identifier], [[NSNumber alloc] initWithInt:1], @"ID not correct.");
    STAssertEqualObjects([event title], @"test party", @"Title not correct.");
    STAssertEqualObjects([event event_description], description, @"Description not correct.");
    STAssertEqualObjects([event location], location, @"Location not correct.");
    STAssertEqualObjects([event address], address, @"Address not correct.");
    STAssertEqualObjects([event image_url], image_url, @"Image url not correct.");
    
    NSLog(@"Date is %@.\n", [[event date] description]);
    
    
    //Test modify
    [dictionary setObject:@"new description" forKey:EVENT_DESCRIPTION];
    
    STAssertNotNil([Event eventWithDictionary:dictionary fromContext:context], @"Response \
                   nil for a complete dictionary.");
    
    request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    predicate = [NSPredicate predicateWithFormat:@"identifier == %@",
                               [dictionary objectForKey:@"id"]];
    [request setPredicate:predicate];
    
    error = nil;
    array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"%@", error);
    }
    STAssertEquals([array count], 1U, @"More than one event for one id found. Count was %d.", [array count]);
    event = [array objectAtIndex:0];
    
    STAssertEqualObjects([event identifier], [[NSNumber alloc] initWithInt:1], @"ID not correct.");
    STAssertEqualObjects([event title], @"test party", @"Title not correct.");
    STAssertEqualObjects([event event_description], @"new description", @"Description not correct.");
    STAssertEqualObjects([event location], location, @"Location not correct.");
    STAssertEqualObjects([event address], address, @"Address not correct.");
    STAssertEqualObjects([event image_url], image_url, @"Image url not correct.");

}
@end

/*"id": 24,
 "title": "Aalto Party",
 "date": "2003-02-04 15:30:00 +02:00", 
 "description": 
 "Awesome party with a lot of drinks",
 "location": 
 "(24.4444,34.4444)",
 "address": "Jamerantaival, 23",
 "image_url": "http://image_url"*/