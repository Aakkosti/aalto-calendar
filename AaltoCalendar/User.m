//
//  User.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 30/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "User.h"
#import "Event.h"


@implementation User

@dynamic identifier;
@dynamic isCurrentUser;
@dynamic username;
@dynamic events;
@dynamic toGoEvents;

@end
