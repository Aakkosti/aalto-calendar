//
//  DataRequest.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 1/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	httpMethodGet = 0,
	httpMethodPost = 1,
	httpMethodPut = 2,
	httpMethodDelete = 3
} httpMethod;

@interface DataRequest : NSObject
@property (strong, readwrite) NSURL* url;
@property (readwrite) httpMethod method;
@property (strong, readwrite) NSData* body;
@property (strong, readwrite) NSURLCredential* credentials;
@property (strong, readwrite) NSDictionary* headers;
- (void)fetch:(void(^)(NSData* data))callbackBlock;
@end
