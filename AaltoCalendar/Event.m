//
//  Event.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 3/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "Event.h"
#import "User.h"


@implementation Event

@dynamic address;
@dynamic changed;
@dynamic date;
@dynamic event_description;
@dynamic identifier;
@dynamic image_identifier;
@dynamic image_url;
@dynamic location;
@dynamic owner_username;
@dynamic title;
@dynamic owner;
@dynamic participants;
@dynamic iCalIdentifier;


@end
