//
//  Event.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 3/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class User;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSNumber * changed;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * event_description;
@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSNumber * image_identifier;
@property (nonatomic, retain) NSString * image_url;
@property (nonatomic, retain) NSString * location;
@property (nonatomic, retain) NSString * owner_username;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) User *owner;
@property (nonatomic, retain) NSSet *participants;
@property (nonatomic, retain) NSString *iCalIdentifier;
@end

@interface Event (CoreDataGeneratedAccessors)

- (void)addParticipantsObject:(User *)value;
- (void)removeParticipantsObject:(User *)value;
- (void)addParticipants:(NSSet *)values;
- (void)removeParticipants:(NSSet *)values;

@end
