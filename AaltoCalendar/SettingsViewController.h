//
//  SettingsViewController.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 26/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
@class SettingsViewController;

@protocol SettingsViewControllerDelegate <NSObject>
- (void)settingsViewController:(SettingsViewController*)controller didSignOutUser:(User*)user;
@end

@interface SettingsViewController : UIViewController
@property (strong, readwrite) User* user;
@property (weak, readwrite) id delegate;
@end
