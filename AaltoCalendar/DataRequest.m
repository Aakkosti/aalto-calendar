//
//  DataRequest.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 1/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "DataRequest.h"

static NSDictionary* httpMethods = nil;

@interface DataRequest ()
@property (strong, readwrite) void (^callbackBlock)(NSData* data);
@property (strong, readwrite) NSMutableData* mutableData;
@end

@implementation DataRequest

+ (void)initialize {
	httpMethods = [[NSDictionary alloc] initWithObjectsAndKeys: @"GET", [[NSNumber alloc] initWithInt:0],
				   @"POST", [[NSNumber alloc] initWithInt:1],
				   @"PUT", [[NSNumber alloc] initWithInt:2],
				   @"DELETE",[[NSNumber alloc] initWithInt:3],
				   nil];
}

- (id)init {
	self = [super init];
	if (self) {
		_url = nil;
		_body = nil;
		_method = httpMethodGet;
		_callbackBlock = nil;
		_credentials = nil;
		_headers = [[NSDictionary alloc] initWithObjectsAndKeys:@"application/json", @"Accept", nil];
	}
	return self;
}

- (void)fetch:(void (^)(NSData* data))callbackBlock {
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
	dispatch_async(queue, ^{
		NSMutableURLRequest* httpRequest = [[NSMutableURLRequest alloc] initWithURL:self.url];
		httpRequest.HTTPMethod = [httpMethods objectForKey:[[NSNumber alloc] initWithInt:self.method]];
		httpRequest.HTTPBody = self.body;
		NSEnumerator* enumerator = self.headers.keyEnumerator;
		NSString* key;
		while ((key = enumerator.nextObject)) {
			[httpRequest setValue:[self.headers objectForKey:key] forHTTPHeaderField:key];
		}
		[httpRequest setCachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:httpRequest delegate:self];

        if (!connection) {
            callbackBlock(nil);
        }
        else {
            self.callbackBlock = callbackBlock;
            self.mutableData = [[NSMutableData alloc] init];
            CFRunLoopRun();
        }
	});
}

//////////////////////////////////////////////////////////////////////
#pragma mark - NSURLConnectionDelegate
//////////////////////////////////////////////////////////////////////

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.mutableData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	dispatch_async(dispatch_get_main_queue(), ^{
		self.callbackBlock(nil);
	});
    CFRunLoopStop(CFRunLoopGetCurrent());
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	dispatch_async(dispatch_get_main_queue(), ^{
		self.callbackBlock(self.mutableData);
	});
    CFRunLoopStop(CFRunLoopGetCurrent());
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	NSURLProtectionSpace* protectionSpace = [challenge protectionSpace];
	if ([[protectionSpace authenticationMethod] isEqualToString:@"NSURLAuthenticationMethodDefault"] && [protectionSpace.realm isEqualToString:@"aalto-calendar"]){
		[challenge.sender useCredential:self.credentials forAuthenticationChallenge:challenge];
	}
	
	else {
		[challenge.sender performDefaultHandlingForAuthenticationChallenge:challenge];
	}
}

@end
