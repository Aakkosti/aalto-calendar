//
//  RootViewController.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 25/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "RootViewController.h"
#import "CoreDataManager.h"
#import "User.h"
#import "AllEventsViewController.h"
#import "MyEventsViewController.h"
#import "GoToEventsViewController.h"
#import "SettingsViewController.h"
#import "Synchronizer.h"
#import "Config.h"
#import "LoadingView.h"
#import "Info.h"

@interface RootViewController ()
@property (strong, readonly) UIView* mainView;
@property (strong, readwrite) UITabBarController* tabBarController;
@property (strong, readwrite) User* user;
@property (strong, readwrite) Synchronizer* synchronizer;
@property (strong, readwrite) LoadingView* loadingView;
@end

@implementation RootViewController
@synthesize mainView = _mainView,
tabBarController = _tabBarController;

- (id)init {
	self = [super init];
	if (self) {
		_synchronizer = [Synchronizer sharedSynchronizer];
		NSNotificationCenter* notificationCenter = [NSNotificationCenter defaultCenter];
		[notificationCenter addObserver:self selector:@selector(synchronizerDidStartSyncing) name:DID_START_SYNCHRONIZING object:nil];
		[notificationCenter addObserver:self selector:@selector(synchronizerDidFinishSyncing) name:DID_FINISH_SYNCHRONIZING object:nil];
		[notificationCenter addObserver:self selector:@selector(synchronizerDidFailSyncing) name:DID_FAIL_SYNCHRONIZING object:nil];
		
	}
	return self;
}

- (UIView*)mainView {
	if (!_mainView) {
		_mainView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].applicationFrame];
	}
	return _mainView;
}

- (void)start {
	CoreDataManager* manager = [CoreDataManager sharedManager];
	NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:@"User"];
	NSPredicate* predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"isCurrentUser", [NSNumber numberWithInt:1]];
	request.predicate = predicate;
	
	NSError* error = nil;
	NSArray* fetchResult = [manager.moc executeFetchRequest:request error:&error];
	if (error) {
		UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Oup"
															message:@"Something went wrong. Please try again"
														   delegate:self
												  cancelButtonTitle:nil
												  otherButtonTitles:@"Ok", nil];
		[alertView show];
		return;
	}
	if (fetchResult.count > 1) {
		NSLog(@"too many currentUsers");
		return;
	}
	if (fetchResult.count == 1) {
		self.user = [fetchResult objectAtIndex:0];
		self.synchronizer.user = self.user;
		[self showViewController:self.tabBarController];
		[self.synchronizer synchronize];
	}
	else {
		SignUpViewController* controller = [[SignUpViewController alloc] init];
		controller.delegate = self;
		UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
		[self showViewController:navigationController];
	}
}

- (BOOL)isDataOutOfDate {
	NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:@"Info"];
	NSError* error = nil;
	CoreDataManager* manager = [CoreDataManager sharedManager];
	NSArray* array = [manager.moc executeFetchRequest:request error:&error];
	if (error) {
		NSLog(@"couldn't fetch info object: %@", error);
		return YES;
	}
	Info* info;
	int nbDaysSinceLastSync;
	if (array.count == 0) {
		info = [NSEntityDescription insertNewObjectForEntityForName:@"Info"
											 inManagedObjectContext:manager.moc];
		nbDaysSinceLastSync = -1;
	}
	else {
		info = [array objectAtIndex:0];
		nbDaysSinceLastSync = [info.lastSync timeIntervalSinceNow] / (3600*24);
	}
	info.lastSync = [NSDate date];
	[manager saveToDiskAsynchronously:YES];
	return (nbDaysSinceLastSync > 10 || nbDaysSinceLastSync == -1);
}

- (void)showViewController:(UIViewController*)controller {
	[self addChildViewController:controller];
	[self.view addSubview:controller.view];
	[controller didMoveToParentViewController:self];
}

- (void)hideViewController:(UIViewController*)controller {
	[controller willMoveToParentViewController:nil];
	[controller.view removeFromSuperview];
	[controller removeFromParentViewController];
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self.view addSubview:self.mainView];
}


- (void)setTabBarController:(UITabBarController *)tabBarController {
	_tabBarController = tabBarController;
}

- (UITabBarController*)tabBarController {
	if (!_tabBarController) {
		_tabBarController = [[UITabBarController alloc] init];
	}
	AllEventsViewController* allEventsController = [[AllEventsViewController alloc] initWithStyle:UITableViewStylePlain];
	MyEventsViewController* myEventsController = [[MyEventsViewController alloc] initWithStyle:UITableViewStylePlain];
	GoToEventsViewController* goToEventsController = [[GoToEventsViewController alloc] initWithStyle:UITableViewStylePlain];
	SettingsViewController* settingsController = [[SettingsViewController alloc] init];
	settingsController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Settings" image:[UIImage imageNamed:@"settings.png"] tag:0];

	settingsController.user = self.user;
	settingsController.delegate = self;
	myEventsController.user = self.user;
	allEventsController.user = self.user;
	goToEventsController.user = self.user;
	UINavigationController* allEventsNavController = [[UINavigationController alloc] initWithRootViewController:allEventsController];
	allEventsNavController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"All" image:[UIImage imageNamed:@"all.png"] tag:0];
	UINavigationController* myEventsNavController = [[UINavigationController alloc] initWithRootViewController:myEventsController];
	myEventsNavController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Mine" image:[UIImage imageNamed:@"mine.png"] tag:0];
	UINavigationController* goToEventsNavController = [[UINavigationController alloc] initWithRootViewController:goToEventsController];
	goToEventsNavController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"GoTo" image:[UIImage imageNamed:@"goto.png"] tag:0];
	
	NSArray* controllers = [NSArray arrayWithObjects:allEventsNavController,
							myEventsNavController,
							goToEventsNavController,
							settingsController,
							nil];
	[_tabBarController setViewControllers:controllers animated:NO];
	return _tabBarController;
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - SignUpViewControllerDelegate

- (void)signUpViewController:(SignUpViewController *)controller didSignUpUser:(User *)user {
	self.user = user;
	[self hideViewController:controller.navigationController];
	if ([self isDataOutOfDate])
		self.loadingView = [[LoadingView alloc] initWithFrame:self.view.bounds];
	else
		[self showViewController:self.tabBarController];
	self.synchronizer.user = self.user;
	[self.synchronizer synchronize];
}

#pragma mark - SignInViewControllerDelegate

- (void)signInViewController:(SignInViewController *)controller didSignInUser:(User *)user {
	self.user = user;
	[self hideViewController:controller.navigationController];
	if ([self isDataOutOfDate])
		self.loadingView = [[LoadingView alloc] initWithFrame:self.view.bounds];
	else
		[self showViewController:self.tabBarController];
	self.synchronizer.user = self.user;
	[self.synchronizer synchronize];
}

#pragma mark - SettingsViewControllerDelegate

- (void)settingsViewController:(SettingsViewController *)controller didSignOutUser:(User *)user {
	self.user = nil;
	[self hideViewController:controller];
	SignInViewController* signInController = [[SignInViewController alloc] init];
	signInController.delegate = self;
	UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:signInController];
	[self showViewController:navigationController];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	[alertView  dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

#pragma mark - Handlers

- (void)synchronizerDidStartSyncing {
	if (self.loadingView) {
		self.loadingView.label.text = @"Loading";
		[self.view addSubview:self.loadingView];
	}
}

- (void)synchronizerDidFinishSyncing {
	if (self.loadingView) {
		[self.loadingView removeFromSuperview];
		self.loadingView = nil;
		[self showViewController:self.tabBarController];
	}
}

- (void)synchronizerDidFailSyncing {
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
														message:@"Sorry, the synchronization failed. Check your internet connection and try again"
													   delegate:nil
											  cancelButtonTitle:@"Ok"
											  otherButtonTitles:nil];
	[alertView show];
}

@end
