//
//  AllEventsViewController.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 26/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface AllEventsViewController : UITableViewController <NSFetchedResultsControllerDelegate>
@property (strong, readwrite) User* user;
@end
