//
//  EventViewController.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 30/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "EventViewController.h"
#import "Synchronizer.h"
#import "CoreDataManager.h"
#import "Event.h"
#import "TextField.h"
#import "Button.h"

#define kNavigationBarColor [UIColor colorWithRed:0.8010 green:0.0550 blue:0.0246 alpha:1.0000]

@interface EventViewController ()
@property (strong, readwrite) UIScrollView* scrollView;
@property (strong, readwrite) UITextField* titleField;
@property (strong, readwrite) UITextField* addressField;
@property (strong, readwrite) UITextView* descriptionView;
@property (strong, readwrite) UIImageView* descriptionImageView;
@property (strong, readwrite) UILabel* descriptionPlaceHolder;
@property (strong, readwrite) UINavigationBar* navigationBar;
@property (strong, readwrite) UIDatePicker* datePicker;
@property (strong, readwrite) UILabel* dateLabel;
@property (strong, readwrite) UIButton* deleteButton;
@end

@implementation EventViewController

- (id)init {
	self = [super init];
	if (self) {
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.scrollView = [[UIScrollView alloc] init];
	[self.view addSubview:self.scrollView];
	self.view.backgroundColor = [UIColor colorWithRed:0.9135 green:0.9135 blue:0.9135 alpha:1.0000];
	
	self.titleField = [[TextField alloc] initWithStyle:TextFieldTop];
	self.titleField.placeholder = @"Title";
	self.titleField.delegate = self;
	[self.scrollView addSubview:self.titleField];
	
	self.addressField = [[TextField alloc] initWithStyle:TextFieldBot];
	self.addressField.placeholder = @"Address";
	self.addressField.delegate = self;
	[self.scrollView addSubview:self.addressField];
	
	UIImage* fieldImage = [[UIImage imageNamed:@"full-textfield.png"]
						   stretchableImageWithLeftCapWidth:20
						   topCapHeight:20];
	self.descriptionImageView = [[UIImageView alloc] initWithImage:fieldImage];
	
	[self.scrollView addSubview:self.descriptionImageView];
	self.descriptionPlaceHolder = [[UILabel alloc] init];
	self.descriptionPlaceHolder.text = @"Description";
	self.descriptionPlaceHolder.font = self.titleField.font;
	self.descriptionPlaceHolder.textColor = [UIColor colorWithRed:0.7137 green:0.7137 blue:0.7137 alpha:1.0000];
	self.descriptionPlaceHolder.backgroundColor = [UIColor clearColor];
	
	[self.scrollView addSubview:self.descriptionPlaceHolder];
	self.descriptionView = [[UITextView alloc] init];
	self.descriptionView.backgroundColor = [UIColor clearColor];
	self.descriptionView.font = self.titleField.font;
	self.descriptionView.delegate = self;
	
	[self.scrollView addSubview:self.descriptionView];
	self.datePicker = [[UIDatePicker alloc] init];
	self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
	self.datePicker.minimumDate = [NSDate date];
	self.datePicker.maximumDate = [NSDate dateWithTimeIntervalSinceNow:3600*24*365];
	[self.datePicker sizeToFit];
	[self.scrollView addSubview:self.datePicker];
	
	UIBarButtonItem* saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save"
																   style:UIBarButtonItemStyleBordered
																  target:self
																  action:@selector(saveButtonWasClicked)];
	
	if (self.event) {
		self.titleField.text = self.event.title;
		self.addressField.text = self.event.address;
		self.descriptionView.text = self.event.event_description;
		self.descriptionPlaceHolder.hidden = YES;
		[self.datePicker setDate:self.event.date animated:NO];
		self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
		UIImage* image = [[UIImage imageNamed:@"delete-button.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:0];
		[self.deleteButton setBackgroundImage:image forState:UIControlStateNormal];
		[self.deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[self.deleteButton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
		[self.deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
		[self.deleteButton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateHighlighted];
		[self.deleteButton setTitle:@"Delete" forState:UIControlStateNormal];
		self.deleteButton.titleLabel.shadowOffset = CGSizeMake(0, 1);
		[self.deleteButton addTarget:self
							  action:@selector(deleteButtonWasClicked)
					forControlEvents:UIControlEventTouchUpInside];
		[self.scrollView addSubview:self.deleteButton];
		self.navigationItem.rightBarButtonItem = saveButton;
	}
	else {
		self.navigationBar = [[UINavigationBar alloc] init];
		self.navigationBar.tintColor = kNavigationBarColor;
		UINavigationItem* item = [[UINavigationItem alloc] initWithTitle:nil];
		UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
																		 style:UIBarButtonItemStyleBordered
																		target:self
																		action:@selector(cancelWasClicked)];
		item.leftBarButtonItem = cancelButton;
		item.rightBarButtonItem = saveButton;
		[self.navigationBar pushNavigationItem:item animated:NO];
		[self.navigationBar sizeToFit];
		[self.view addSubview:self.navigationBar];
	}
}

- (void)viewWillLayoutSubviews {
	CGSize fieldSize = CGSizeMake(300, 44);
	int topBorder = 10;
	int leftBorder = (self.view.frame.size.width - fieldSize.width)/2;
	CGSize viewSize = self.view.bounds.size;
	CGSize navBarSize;
	if (self.event) {
		navBarSize = CGSizeZero;
	}
	else {
		navBarSize= self.navigationBar.frame.size;
	}
	
	self.titleField.frame = CGRectMake(leftBorder,
									   topBorder,
									   fieldSize.width,
									   fieldSize.height);
	self.addressField.frame = CGRectMake(leftBorder,
										 CGRectGetMaxY(self.titleField.frame),
										 fieldSize.width,
										 fieldSize.height);
	
	self.descriptionImageView.frame = CGRectMake(leftBorder,
												 CGRectGetMaxY(self.addressField.frame) + topBorder,
												 fieldSize.width,
												 200);
	
	self.descriptionView.frame = CGRectInset(self.descriptionImageView.frame, 5, 5);
	[self.descriptionPlaceHolder sizeToFit];
	self.descriptionPlaceHolder.frame = CGRectMake(self.descriptionView.frame.origin.x + 7,
												   self.descriptionView.frame.origin.y + 9,
												   self.descriptionPlaceHolder.frame.size.width,
												   self.descriptionPlaceHolder.frame.size.height);
	CGRect datePickerFrame = self.datePicker.frame;
	datePickerFrame.origin.x = 0;
	datePickerFrame.origin.y = CGRectGetMaxY(self.descriptionImageView.frame) + topBorder;
	self.datePicker.frame = datePickerFrame;
	
	CGSize buttonSize = CGSizeMake(300, 44);
	CGSize scrollContentSize = CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(self.datePicker.frame));
	if (self.deleteButton) {
		self.deleteButton.frame = CGRectMake((viewSize.width - buttonSize.width) / 2,
											 CGRectGetMaxY(self.datePicker.frame) + topBorder,
											 buttonSize.width,
											 buttonSize.height);
		scrollContentSize.height += + 2 * topBorder + buttonSize.height;
	}
	
	self.scrollView.frame = CGRectMake(0,
									   navBarSize.height,
									   viewSize.width,
									   viewSize.height - navBarSize.height);
	self.scrollView.contentSize = scrollContentSize;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[self.view endEditing:YES];
	return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
	if (textView.text.length > 1)
		self.descriptionPlaceHolder.hidden = YES;
	else
		self.descriptionPlaceHolder.hidden = NO;
}

#pragma mark - Handlers

- (void)saveButtonWasClicked {
	Synchronizer* syncer = [Synchronizer sharedSynchronizer];
	CoreDataManager* manager = [CoreDataManager sharedManager];
	BOOL needUpdate = self.event != nil;
	if (!self.event) {
		self.event = [NSEntityDescription insertNewObjectForEntityForName:@"Event"
													 inManagedObjectContext:manager.moc];
	}
	self.event.title = self.titleField.text;
	self.event.address = self.addressField.text;
	self.event.event_description = self.descriptionView.text;
	self.event.date = self.datePicker.date;
	self.event.location = @"(34,34)";
	self.event.image_url = @"url";
	self.event.owner = self.user;
	if (needUpdate) {
		[syncer updateEvent:self.event];
	}
	else {
		[syncer addEvent:self.event];
	}
	[manager saveToDiskAsynchronously:YES];
	[self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)deleteButtonWasClicked {
    Synchronizer* syncer = [Synchronizer sharedSynchronizer];
	[syncer deleteEvent:self.event];
}

- (void)cancelWasClicked {
	[self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

@end
