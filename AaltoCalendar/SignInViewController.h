//
//  SignInViewController.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 26/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
@class SignInViewController;

@protocol SignInViewControllerDelegate <NSObject>
- (void)signInViewController:(SignInViewController*)controller didSignInUser:(User*)user;
@end

@interface SignInViewController : UIViewController <UIAlertViewDelegate, UITextFieldDelegate>
@property (weak, readwrite) id delegate;
@end
