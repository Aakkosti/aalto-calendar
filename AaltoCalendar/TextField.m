//
//  TextField.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 1/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "TextField.h"

@implementation TextField

- (id)initWithStyle:(TextFieldStyle)style {
	self = [super init];
	if (self) {
		UIImage* image;
		if (style == TextFieldFull) {
			image = [UIImage imageNamed:@"full-textfield.png"];
		}
		else if (style == TextFieldTop) {
			image = [UIImage imageNamed:@"top-textfield.png"];
		}
		else if (style == TextFieldMid) {
			image = [UIImage imageNamed:@"middle-textfield.png"];
		}
		else if (style == TextFieldBot) {
			image = [UIImage imageNamed:@"bottom-textfield.png"];
		}
		self.background = [image stretchableImageWithLeftCapWidth:20 topCapHeight:20];
		self.frame = CGRectMake(0,
								0,
								image.size.width,
								image.size.height);
		self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
		self.borderStyle = UITextBorderStyleNone;
		self.autocapitalizationType = UITextAutocapitalizationTypeNone;
		self.returnKeyType = UIReturnKeyDone;
	}
	return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
	return CGRectInset(bounds, 10, 5);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
	return CGRectInset(bounds, 10, 5);
}

@end
