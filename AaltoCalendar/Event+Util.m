//
//  Event+Util.m
//  AaltoCalendar
//
//  Created by Jaakko Kaisanlahti on 2013/03/22.
//  Copyright (c) 2013年 Group 14. All rights reserved.
//

#import "Event+Util.h"
#import "Config.h"
#import "User.h"
#import <CoreLocation/CoreLocation.h>


#define JSON_KEYS [NSArray arrayWithObjects: EVENT_ID, EVENT_TITLE, EVENT_DATE, EVENT_DESCRIPTION, EVENT_LOCATION, EVENT_ADDRESS, EVENT_IMAGE_URL, nil]

@implementation Event (Util)

+ (Event *)eventWithDictionary:(NSDictionary *)dictionary fromContext:(NSManagedObjectContext *)context {
    if (dictionary == nil) {
        return nil;
    }
    //Dictionary doesn't contain all the required fields.
    for (NSString *key in JSON_KEYS) {
        if ([dictionary objectForKey:key] == nil) {
            return nil;
        }
    }
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:
                                              @"Event"
                                                         inManagedObjectContext:
                                              context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"identifier == %@",
                               [dictionary objectForKey:EVENT_ID]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
		NSLog(@"%@", error);
        return nil;
    }
    Event *event;
    //Event already exists.
    if ([array count] == 1) {
        event = [array objectAtIndex:0];
    }
    //More than one event for id found. Database is inconsistent. Aborting.
    else if ([array count] > 1) {
        NSLog(@"More than one Event for id %@ found.", [dictionary objectForKey:EVENT_ID]);
        return nil;
    }
    //New event.
    else {
        event = [NSEntityDescription insertNewObjectForEntityForName:@"Event"
                                              inManagedObjectContext:context];
    }
    
    [event setIdentifier:[dictionary objectForKey:EVENT_ID]];
    [event setTitle:[dictionary objectForKey:EVENT_TITLE]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //Example date string2003-02-04 15:30:00 +02:00
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    [event setDate:[formatter dateFromString:[dictionary objectForKey:EVENT_DATE]]];
    [event setEvent_description:[dictionary objectForKey:EVENT_DESCRIPTION]];
    [event setLocation:[dictionary objectForKey:EVENT_LOCATION]];
	if (!([[dictionary objectForKey:EVENT_IMAGE_ID] isEqual:[NSNull null]])) {
		[event setImage_identifier:[dictionary objectForKey:EVENT_IMAGE_ID]];
	}
    [event setAddress:[dictionary objectForKey:EVENT_ADDRESS]];
	if (!([[dictionary objectForKey:EVENT_IMAGE_URL] isEqual:[NSNull null]])) {
		[event setImage_url:[dictionary objectForKey:EVENT_IMAGE_URL]];
	}
	[event setOwner_username:[dictionary objectForKey:EVENT_OWNER_USERNAME]];
    NSEntityDescription *userEntity = [NSEntityDescription entityForName:
                                       @"User"
                                                  inManagedObjectContext:
                                       context];
    NSFetchRequest *userRequest = [[NSFetchRequest alloc] init];
    [userRequest setEntity:userEntity];
    NSPredicate * userPredicate = [NSPredicate predicateWithFormat:@"identifier == %@",
                               [dictionary objectForKey:EVENT_USER_ID]];
    [userRequest setPredicate:userPredicate];
    
    error = nil;
    array = [context executeFetchRequest:userRequest error:&error];
    // If count = 0, the owner is a user on a different phone.
    // If count > 1, error.
    if ([array count] > 1) {
        //Shouldn't happen.
        NSLog(@"More than one user with user_id %@ in the database.\n",
              [dictionary objectForKey:EVENT_USER_ID]);
        abort();
    }
    // If count = 1, user is on this phone.

    else if ([array count] == 1) {
        [event setOwner:[array objectAtIndex:0]];
    }
    return event;
}
- (NSDictionary *)dictionary {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    if ([self identifier]) {
        [dictionary setObject:[self identifier] forKey:EVENT_ID];
    }
    //Dirty hack to get +3:00 timezone.
    NSLocale *loc = [[NSLocale alloc] initWithLocaleIdentifier:@"fr_BE"];
    [formatter setLocale:loc];
	NSString* dateString = [formatter stringFromDate:[self date]];
	dateString = [dateString stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
    dateString = [dateString stringByReplacingOccurrencesOfString:@"UTC" withString:@""];
    [dictionary setObject:[self title] forKey:EVENT_TITLE];
    [dictionary setObject:dateString forKey:EVENT_DATE];
    [dictionary setObject:[self event_description] forKey:EVENT_DESCRIPTION];
    [dictionary setObject:[self location] forKey:EVENT_LOCATION];
    [dictionary setObject:[self address] forKey:EVENT_ADDRESS];
	if ([self image_identifier]) {
		[dictionary setObject:[self image_identifier] forKey:EVENT_IMAGE_ID];
	}
	else {
		[dictionary setObject:[NSNull null] forKey:EVENT_IMAGE_ID];
	}
	if ([self image_url]) {
		[dictionary setObject:[self image_url] forKey:EVENT_IMAGE_URL];
	}
	else {
		[dictionary setObject:[NSNull null] forKey:EVENT_IMAGE_URL];
	}
    if ([self owner] != nil) {
        [dictionary setObject:[[self owner] identifier] forKey:EVENT_USER_ID];
    }
    
    return dictionary;
}

#pragma mark - MKAnnotation protocol

- (CLLocationCoordinate2D)coordinate {
	NSString* withoutParentheses = [self.location stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];
	NSArray* components = [withoutParentheses componentsSeparatedByString:@","];
	return CLLocationCoordinate2DMake(((NSString*)[components objectAtIndex:0]).floatValue,
																	((NSString*)[components objectAtIndex:1]).floatValue);
}

- (NSString*)subtitle {
	return nil;
}
@end
