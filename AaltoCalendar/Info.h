//
//  Info.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 29/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Info : NSManagedObject

@property (nonatomic, retain) NSDate * lastSync;

@end
