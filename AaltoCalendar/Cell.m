//
//  Cell.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 29/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "Cell.h"
#import "DataRequest.h"
#import "FileManager.h"
#import "Config.h"

#define kTitleFont [UIFont boldSystemFontOfSize:16]
#define kSubtitleFont [UIFont systemFontOfSize:14]
#define kInfoFont [UIFont systemFontOfSize:14]


@interface Cell	()
@property (strong, readwrite) NSURL* imageUrl;
@property (strong, readwrite) UILabel* title;
@property (strong, readwrite) UILabel* subtitle;
@property (strong, readwrite) UILabel* topRightInfo;
@property (strong, readwrite) UILabel* bottomRightInfo;
@property (strong, readwrite) UIImageView* myImageView;
@property (strong, readwrite) FileManager* fileManager;
@end

@implementation Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
		_fileManager = [FileManager sharedManager];
		self.backgroundView = [[UIView alloc] initWithFrame:self.bounds];
		self.backgroundView.backgroundColor = [UIColor whiteColor];
		CGSize boundsSize = self.bounds.size;
		boundsSize.height = kCellHeight;
		NSString* dummyString = @"dummy";
		CGSize titleSize = [dummyString sizeWithFont:kTitleFont];
		titleSize.width = 150;
		_myImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,
																	 0,
																	 kCellHeight,
																	 kCellHeight)];
		_myImageView.contentMode = UIViewContentModeScaleAspectFill;
		_myImageView.clipsToBounds = YES;
		[self.contentView addSubview:_myImageView];
		_title = [[UILabel alloc] initWithFrame:CGRectMake(kCellHeight + 5,
														   boundsSize.height / 2 - titleSize.height,
														   titleSize.width,
														   titleSize.height)];
		_title.font = kTitleFont;
		_title.textAlignment = NSTextAlignmentLeft;
		_title.lineBreakMode = NSLineBreakByTruncatingTail;
		_title.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		[self.contentView addSubview:_title];
		CGSize subtitleSize = [dummyString sizeWithFont:kSubtitleFont];
		subtitleSize.width = 150;
		_subtitle = [[UILabel alloc] initWithFrame:CGRectMake(kCellHeight + 5,
															  boundsSize.height / 2,
															  subtitleSize.width,
															  subtitleSize.height)];
		_subtitle.font = kSubtitleFont;
		_subtitle.textAlignment = NSTextAlignmentLeft;
		_subtitle.lineBreakMode = NSLineBreakByTruncatingTail;
		_subtitle.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		[self.contentView addSubview:_subtitle];
		CGSize infoSize = [dummyString sizeWithFont:kInfoFont];
		infoSize.width = boundsSize.width - kCellHeight - titleSize.width;
		_topRightInfo = [[UILabel alloc] initWithFrame:CGRectMake(boundsSize.width - infoSize.width - 10,
																  boundsSize.height / 2 - infoSize.height,
																  infoSize.width,
																  infoSize.height)];
		_topRightInfo.font = kInfoFont;
		_topRightInfo.textAlignment = NSTextAlignmentRight;
		_topRightInfo.lineBreakMode = NSLineBreakByTruncatingMiddle;
		_topRightInfo.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
		[self.contentView addSubview:_topRightInfo];
		_bottomRightInfo = [[UILabel alloc] initWithFrame:CGRectMake(boundsSize.width - infoSize.width - 10,
																	 boundsSize.height / 2,
																	 infoSize.width,
																	 infoSize.height)];
		_bottomRightInfo.font = kInfoFont;
		_bottomRightInfo.textAlignment = NSTextAlignmentRight;
		_bottomRightInfo.lineBreakMode = NSLineBreakByTruncatingMiddle;
		_bottomRightInfo.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
		[self.contentView addSubview:_bottomRightInfo];
		self.selectionStyle = UITableViewCellSelectionStyleGray;
		self.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
		
		[self addObserver:self forKeyPath:@"event" options:NSKeyValueObservingOptionOld context:nil];
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if ([keyPath isEqualToString:@"event"]){
		self.myImageView.image = [UIImage imageNamed:@"no-image.png"];
		self.title.text = self.event.title;
		self.subtitle.text = self.event.address;
		NSDate* date = self.event.date;
		NSCalendar* calendar = [NSCalendar currentCalendar];
		NSDateComponents* components = [calendar components:NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit
												   fromDate:date];
		
		self.topRightInfo.text = [NSString stringWithFormat:@"%02d/%02d", components.day, components.month];
		self.bottomRightInfo.text = [NSString stringWithFormat:@"%02d:%02d", components.hour, components.minute];
		
		UIImage* image = [self.fileManager.cache objectForKey:self.event.image_identifier];
		if (image) {
			self.myImageView.image = image;
		}
		else {
			NSNumber* identifier = self.event.image_identifier;
			NSString* imageUrl = self.event.image_url;
			dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
			dispatch_async(queue, ^{
				FileManager* manager = [FileManager sharedManager];
				NSData* data = [manager dataFromDiskWithIdentifier:identifier.stringValue];
				if (data) {
					dispatch_async(dispatch_get_main_queue(), ^{
						//NSData is threadsafe
						UIImage* image = [UIImage imageWithData:data];
						[self.fileManager.cache setObject:image forKey:identifier];
						self.myImageView.image = image;
					});
				}
				else {
					DataRequest* request = [[DataRequest alloc] init];
					request.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", SERVER_URL, imageUrl]];
					request.method = httpMethodGet;
					[request fetch:^(NSData *data) {
						if (!data) {
							return;
						}
						NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
						if ((json && [json objectForKey:@"error"]) || data.length == 0) {
							return;
						}
						UIImage* image = [UIImage imageWithData:data];
						[self.fileManager.cache setObject:image forKey:identifier];
						self.myImageView.image = image;
						
						dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
						dispatch_async(queue, ^{
							FileManager* manager = [FileManager sharedManager];
							[manager writeDataToDisk:data withIdentifier:identifier.stringValue];
						});
					 }];
				}
			});
		}
	}
}

- (void)dealloc {
	[self removeObserver:self forKeyPath:@"event"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
	[super setEditing:editing animated:animated];
}

@end
