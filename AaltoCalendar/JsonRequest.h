//
//  JsonRequest.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 19/03/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataRequest.h"

@interface JsonRequest : NSObject <NSURLConnectionDelegate>
@property (strong, readwrite) NSURL* url;
@property (readwrite) httpMethod method;
@property (strong, readwrite) NSObject* body;
@property (strong, readwrite) NSURLCredential* credentials;

- (void)fetch:(void(^)(NSObject* json))callbackBlock;
@end

////////////////////////////////////////////////////////
// Usage example
////////////////////////////////////////////////////////

/*
JsonRequest* request = [[JsonRequest alloc] init];
NSMutableDictionary* body = [[NSMutableDictionary alloc] init];
[body setObject:@"Aalto Party" forKey:@"title"];
[body setObject:@"2003-02-04 15:30:00 +02:00" forKey:@"date"];
[body setObject:@"Awesome party with a lot of drinks" forKey:@"description"];
[body setObject:@"(24.3242,32.2342)" forKey:@"location"];
[body setObject:@"Jamerantaival, 23" forKey:@"address"];
[body setObject:@"http://my_image_url" forKey:@"image_url"];
request.url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@", SERVER_URL, @"events"]];
request.body = body;
request.method = httpMethodPost;
request.credentials = [[NSURLCredential alloc] initWithUser:@"Seb" password:@"password" persistence:NSURLCredentialPersistenceNone];
void (^callbackBlock)(NSObject* json) = ^(NSObject* json) {
	//This is called on the main thread
	//Do your callback stuff here.
	//json should be an NSDictionary but just check for precaution
};
[request fetch:callbackBlock];
 */
