//
//  LoadingView.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 26/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView
@property (strong, readonly) UILabel* label;
@end
