//
//  Synchronizer.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 25/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "Synchronizer.h"
#import "CoreDataManager.h"
#import "Config.h"
#import "Event+Util.h"
#import "SFHFKeychainUtils.h"
#import "DeletedEvent.h"

@interface Synchronizer ()
@property (readwrite, getter = isSyncing) BOOL syncing;
@end

@implementation Synchronizer
@synthesize delegate = _delegate, user;

static Synchronizer* synchronizer = nil;

+ (Synchronizer*)sharedSynchronizer {
	if (!synchronizer) {
		synchronizer = [[Synchronizer alloc] init];
	}
	return synchronizer;
}

- (void)synchronize {
    //Don't synchronize if already syncing. Can't synchronize without user.
    if (self.syncing || self.user == nil) {
        return;
    }
    self.syncing = YES;
    
    if ([self.delegate respondsToSelector:@selector(synchronizerDidStartSyncing:)]) {
        [self.delegate synchronizerDidStartSyncing:self];
    }
    
    //Push our changes before pulling.
    //[self syncUnsynched];
    
    CoreDataManager *manager = [CoreDataManager sharedManager];
    NSManagedObjectContext *moc = [manager moc];
    
    JsonRequest* request = [[JsonRequest alloc] init];
    request.url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@", SERVER_URL, @"events"]];
    request.method = httpMethodGet;
    
    NSString *username = [self.user username];
    NSError *error = nil;
    NSString *password = [SFHFKeychainUtils getPasswordForUsername:username andServiceName:KEYCHAIN_SERVICE error:&error];
    
    if (error) {
        NSLog(@"Error in password retrieval. %@\n.", error);
        self.syncing = NO;
        if ([self.delegate respondsToSelector:@selector(synchronizerDidFailSyncing:error:)]) {
            [self.delegate synchronizerDidFailSyncing:self error:error];
        }
        return;
    }
    
    request.credentials = [[NSURLCredential alloc] initWithUser:username password:password persistence:NSURLCredentialPersistenceNone];
    void (^callbackBlock)(NSObject* json) = ^(NSObject* json) {
        //Result should be an array of dictionaries.
        if ([json isKindOfClass:[NSArray class]]) {
            //Save all event ids for future use.
            NSMutableArray *ids = [[NSMutableArray alloc] init];
            for (NSDictionary *dictionary in (NSArray *)json) {
                [Event eventWithDictionary:dictionary fromContext:moc];
                [ids addObject:[dictionary objectForKey:EVENT_ID]];
            }
            //Get events.
            NSEntityDescription *entityDescription = [NSEntityDescription entityForName:
                                                      @"Event"
                                                                 inManagedObjectContext:
                                                      moc];
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            [request setEntity:entityDescription];
            
            NSError *error = nil;
            NSArray *array = [moc executeFetchRequest:request error:&error];
            
            if (error) {
                NSLog(@"%@", error);
            }
            //Delete events that weren't in the JSON. (They have been deleted
            //from the server).
            NSMutableArray *arrayCopy = [NSMutableArray arrayWithArray:array];
            for (Event* event in array) {
                for (NSNumber *identifier in ids) {
                    if ([identifier isEqualToNumber:[event identifier]]) {
                        [arrayCopy removeObject:event];
                        break;
                    }
                }
            }
            for (Event *event in arrayCopy) {
                [moc deleteObject:event];
            }

            //Save asynchronously, but call contextFinishedSave when done.
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(contextFinishedSave)
                                                         name:NSManagedObjectContextDidSaveNotification
                                                       object:manager.backgroundMoc];
            [manager saveToDiskAsynchronously:YES];
        }
        //Something is wrong.
        else  {
            NSLog(@"Sync failed. JSON: %@\n", json);
            if ([self.delegate respondsToSelector:@selector(synchronizerDidFailSyncing:error:)]) {
                [self.delegate synchronizerDidFailSyncing:self error:error];
            }
            self.syncing = NO;
        }
    };
    [request fetch:callbackBlock];
}

- (void)contextFinishedSave {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
	dispatch_async(dispatch_get_main_queue(), ^{
		if ([self.delegate respondsToSelector:@selector(synchronizerDidFinishSyncing:)]) {
			[self.delegate synchronizerDidFinishSyncing:self];
			self.syncing = NO;
		}
	});
    
    //Log events to show that we have them.
    /*NSEntityDescription *entityDescription = [NSEntityDescription entityForName:
                                              @"Event"
                                                         inManagedObjectContext:
                                              [[CoreDataManager sharedManager] moc]];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSError *error = nil;
    NSArray *array = [[[CoreDataManager sharedManager] moc] executeFetchRequest:request error:&error];
    for (Event* event in array) {
        NSLog(@"Event id is %@\n", [event identifier]);
    }*/
}

#pragma mark - Adding and updating

- (void)addEvent:(Event*)event {
	[self saveEvent:event withMethod:httpMethodPost];
}

- (void)updateEvent:(Event *)event {
	[self saveEvent:event withMethod:httpMethodPut];
}

- (void)saveEvent:(Event*)event withMethod:(httpMethod)method {
	JsonRequest* request = [[JsonRequest alloc] init];
    if (method == httpMethodPut) {
        request.url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", SERVER_URL, @"events/", [event identifier]]];
    }
    else {
        request.url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@", SERVER_URL, @"events"]];
    }
    request.method = method;
    
    NSString *username = [self.user username];
    NSError *error = nil;
    NSString *password = [SFHFKeychainUtils getPasswordForUsername:username andServiceName:KEYCHAIN_SERVICE error:&error];
    
    if (error) {
        NSLog(@"Error in password retrieval. %@\n.", error);
        return;
    }
    
    request.body = [event dictionary];
    
    request.credentials = [[NSURLCredential alloc] initWithUser:username password:password persistence:NSURLCredentialPersistenceNone];
    void (^callbackBlock)(NSObject* json) = ^(NSObject* json) {
        //Result should be a dictionary with key success
        if ([json isKindOfClass:[NSDictionary class]]) {
            if ([(NSDictionary *)json objectForKey:@"success"]) {
                [event setChanged:[NSNumber numberWithBool:NO]];
            }
            //Something is wrong.
            else  {
                NSLog(@"Update failed. JSON: %@\n", json);
            }
            //A new event got its ID.
            if ([(NSDictionary  *) json objectForKey:EVENT_ID]) {
                [event setIdentifier:[(NSDictionary  *) json objectForKey:EVENT_ID]];
            }
        }
        //Something is wrong.
        else  {
            NSLog(@"Update failed. JSON: %@\n", json);
        }
		CoreDataManager* manager = [CoreDataManager sharedManager];
		[manager saveToDiskAsynchronously:YES];
    };
    [request fetch:callbackBlock];
}

#pragma mark - Deletion

- (void)deleteEvent:(Event *)event {
    JsonRequest* request = [[JsonRequest alloc] init];
    request.url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", SERVER_URL, @"events/", [event identifier]]];
    request.method = httpMethodDelete;
    NSString *username = [self.user username];
    NSError *error = nil;
    NSString *password = [SFHFKeychainUtils getPasswordForUsername:username andServiceName:KEYCHAIN_SERVICE error:&error];
    
    if (error) {
        NSLog(@"Error in password retrieval. %@\n.", error);
        return;
    }
    request.credentials = [[NSURLCredential alloc] initWithUser:username password:password persistence:NSURLCredentialPersistenceNone];
    
    void (^callbackBlock)(NSObject* json) = ^(NSObject* json) {
        
        if ([json isKindOfClass:[NSDictionary class]]) {
            if ([(NSDictionary *)json objectForKey:@"success"]) {
                [[[CoreDataManager sharedManager] moc] deleteObject:event];
            }
            else {
                [NSEntityDescription insertNewObjectForEntityForName:@"DeletedEvent" inManagedObjectContext:[[CoreDataManager sharedManager] moc]];
                [[[CoreDataManager sharedManager] moc] deleteObject:event];
            }
        }
        //Something is wrong.
        else  {
            [NSEntityDescription insertNewObjectForEntityForName:@"DeletedEvent" inManagedObjectContext:[[CoreDataManager sharedManager] moc]];
            [[[CoreDataManager sharedManager] moc] deleteObject:event];
        }
    };
    [request fetch:callbackBlock];
}

- (void)deleteEventWithId:(DeletedEvent *)event {
    JsonRequest* request = [[JsonRequest alloc] init];
    request.url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", SERVER_URL, @"events/", [event identifier]]];
    request.method = httpMethodDelete;
    NSString *username = [self.user username];
    NSError *error = nil;
    NSString *password = [SFHFKeychainUtils getPasswordForUsername:username andServiceName:KEYCHAIN_SERVICE error:&error];
    
    if (error) {
        NSLog(@"Error in password retrieval. %@\n.", error);
        return;
    }
    request.credentials = [[NSURLCredential alloc] initWithUser:username password:password persistence:NSURLCredentialPersistenceNone];
    
    void (^callbackBlock)(NSObject* json) = ^(NSObject* json) {
        
        if ([json isKindOfClass:[NSDictionary class]]) {
            if ([(NSDictionary *)json objectForKey:@"success"]) {
                [[[CoreDataManager sharedManager] moc] deleteObject:event];
            }
        }
    };
    [request fetch:callbackBlock];
}

- (void)uploadImage:(NSData *)image withBlock:(void(^) (NSString *imageUrl))block {
    DataRequest* request = [[DataRequest alloc] init];
    request.url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@", SERVER_URL, @"images/"]];
    request.method = httpMethodPost;
    NSString *username = [self.user username];
    NSError *error = nil;
    NSString *password = [SFHFKeychainUtils getPasswordForUsername:username andServiceName:KEYCHAIN_SERVICE error:&error];
    request.credentials = [[NSURLCredential alloc] initWithUser:username password:password persistence:NSURLCredentialPersistenceNone];
    NSDictionary *headers = [NSDictionary dictionaryWithObjectsAndKeys:@"image/png", @"Content-Type",
                             @"application/json", @"Accept", nil];
    [request setHeaders:headers];
    request.body = image;
    void (^callbackBlock)(NSData* data) = ^(NSData* data) {
        NSObject* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        if ([json isKindOfClass:[NSDictionary class]]) {
            if ([(NSDictionary *)json objectForKey:@"success"]) {
               block([(NSDictionary *)json objectForKey:@"url"]);
            }
        }
    };
    [request fetch:callbackBlock];
}
@end
