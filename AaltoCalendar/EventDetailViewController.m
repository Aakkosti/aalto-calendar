//
//  EventDetailViewController.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 29/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "EventDetailViewController.h"
#import "User.h"
#import "Event+Util.h"
#import "MapViewController.h"
#import "CoreDataManager.h"
#import "Reachability.h"
#import "AddEventViewController.h"
#import "FileManager.h"
#import <EventKit/EventKit.h>

#define kPinIdentifier @"Pin"

@interface EventDetailViewController ()
@property (strong, readwrite) UILabel* dateLabel;
@property (strong, readwrite) UILabel* addressLabel;
@property (strong, readwrite) UILabel* ownerLabel;
@property (strong, readwrite) UITextView* descriptionTextView;
@property (strong, readwrite) MKMapView* mapView;
@property (strong, readwrite) UILabel* titleLabel;
@property (strong, readwrite) UIImageView* topImageView;
@property (strong, readwrite) UIImageView* botImageView;
@property (strong, readwrite) UIScrollView* scrollView;
@property (strong, readwrite) UITapGestureRecognizer* gestureRecognizer;
@property (strong, readwrite) UIView* shareBar;
@property (strong, readwrite) UIButton* iCalButton;
@end

@implementation EventDetailViewController

- (id)init {
	self = [super init];
	if (self) {
		_gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapViewWasClicked)];
		_gestureRecognizer.numberOfTapsRequired = 1;
		_gestureRecognizer.numberOfTouchesRequired = 1;
		_gestureRecognizer.delegate = self;
		self.title = @"Event detail";
	}
	return self;
}

- (UINavigationItem*)navigationItem {
	UINavigationItem* item = [super navigationItem];
	if (self.event.owner == self.user) {
		
		return item;
	}
	UIBarButtonItem* button;
	if ([self.event.participants containsObject:self.user]) {
		button = [[UIBarButtonItem alloc] initWithTitle:@"Leave"
												  style:UIBarButtonItemStyleBordered
												 target:self
												 action:@selector(userDidLeaveEvent)];
	}
	else {
		button = [[UIBarButtonItem alloc] initWithTitle:@"Join"
												  style:UIBarButtonItemStyleBordered
												 target:self
												 action:@selector(userDidJoinEvent)];
	}
	item.rightBarButtonItem = button;
	return item;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.scrollView = [[UIScrollView alloc] init];
	self.scrollView.backgroundColor = [UIColor colorWithRed:0.9135 green:0.9135 blue:0.9135 alpha:1.0000];

	[self.view addSubview:self.scrollView];
	UIImage* topImage = [[UIImage imageNamed:@"top-textfield.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:20];
	UIImage* botImage = [[UIImage imageNamed:@"bottom-textfield.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:20];
	self.topImageView = [[UIImageView alloc] initWithImage:topImage];
	[self.scrollView addSubview:self.topImageView];
	self.botImageView = [[UIImageView alloc] initWithImage:botImage];
	self.botImageView.userInteractionEnabled = YES;
	[self.scrollView addSubview:self.botImageView];
	
	self.titleLabel = [[UILabel alloc] init];
	self.titleLabel.font = [UIFont boldSystemFontOfSize:22];
	self.titleLabel.textAlignment = NSTextAlignmentLeft;
	self.titleLabel.text = self.event.title;
	self.titleLabel.backgroundColor = [UIColor clearColor];
	[self.topImageView addSubview:self.titleLabel];
	
	UIFont* detailFont = [UIFont systemFontOfSize:16];
	self.ownerLabel = [[UILabel alloc] init];
	self.ownerLabel.font = detailFont;
	self.ownerLabel.textAlignment = NSTextAlignmentLeft;
	self.ownerLabel.text = [NSString stringWithFormat:@"Hosted by: %@", self.event.owner_username];
	self.ownerLabel.backgroundColor = [UIColor clearColor];
	[self.topImageView addSubview:self.ownerLabel];
	
	self.dateLabel = [[UILabel alloc] init];
	self.dateLabel.font = detailFont;
	self.dateLabel.textAlignment = NSTextAlignmentLeft;
	NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
	dateFormatter.dateFormat = @"EEEE d MMMM yyyy '\nfrom' HH':'mm";
	self.dateLabel.numberOfLines = 2;
	self.dateLabel.text = [dateFormatter stringFromDate:self.event.date];
	self.dateLabel.backgroundColor = [UIColor clearColor];
	self.dateLabel.textColor = [UIColor colorWithRed:0.1678 green:0.2986 blue:0.5908 alpha:1.0000];
	[self.topImageView addSubview:self.dateLabel];
	
	self.addressLabel = [[UILabel alloc] init];
	self.addressLabel.font = detailFont;
	self.addressLabel.textAlignment = NSTextAlignmentLeft;
	self.addressLabel.text = [NSString stringWithFormat:@"At %@", self.event.address];
	self.addressLabel.backgroundColor = [UIColor clearColor];
	self.addressLabel.numberOfLines = 0;
	[self.topImageView addSubview:self.addressLabel];
	
	self.descriptionTextView = [[UITextView alloc] init];
	self.descriptionTextView.editable = NO;
	self.descriptionTextView.scrollEnabled = YES;
	self.descriptionTextView.userInteractionEnabled = YES;
	self.descriptionTextView.font = detailFont;
	self.descriptionTextView.text = self.event.event_description;
	self.descriptionTextView.backgroundColor = [UIColor clearColor];
	self.descriptionTextView.contentInset = UIEdgeInsetsMake(-11,-8,0,0);
	[self.botImageView addSubview:self.descriptionTextView];
	
	self.mapView = [[MKMapView alloc] init];
	 [self.mapView addAnnotation:self.event];
	 [self.mapView addGestureRecognizer:self.gestureRecognizer];
	 self.mapView.showsUserLocation = NO;
	 self.mapView.scrollEnabled = NO;
	 self.mapView.zoomEnabled = NO;
	 self.mapView.delegate = self;
	 [self.scrollView addSubview:self.mapView];
	if ([self.event.participants containsObject:self.user] || self.event.owner == self.user) {
		self.shareBar = [[UIView alloc] initWithFrame:CGRectMake(0,
																 0,
																 self.view.frame.size.width,
																 44)];
		UIButton* shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
								 UIImage* shareImage = [UIImage imageNamed:@"share-button.png"];
								 [shareButton setImage:shareImage forState:UIControlStateNormal];
		shareButton.frame = CGRectMake(self.view.frame.size.width - shareImage.size.width - 5,
									   7,
									   shareImage.size.width,
									   shareImage.size.height);
		[shareButton addTarget:self action:@selector(loadSocialView) forControlEvents:UIControlEventTouchUpInside];
		[self.shareBar addSubview:shareButton];
		self.shareBar.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
		[self.view addSubview:self.shareBar];
		UIButton* icalButton = [UIButton buttonWithType:UIButtonTypeCustom];
		UIImage* icalImage = [UIImage imageNamed:@"ical-button.png"];
		[icalButton setImage:icalImage forState:UIControlStateNormal];
		icalButton.frame = CGRectMake(self.view.frame.size.width - 2 * shareImage.size.width - 2* 5,
									   7,
									   icalImage.size.width,
									   icalImage.size.height);
		[icalButton addTarget:self action:@selector(saveToICal) forControlEvents:UIControlEventTouchUpInside];
		if (self.event.iCalIdentifier) {
			icalButton.enabled = NO;
		}
		self.iCalButton = icalButton;
		[self.shareBar addSubview:icalButton];
		self.scrollView.frame = CGRectMake(0, 44, self.view.bounds.size.width, self.view.bounds.size.height - 88);
	}
	else {
		self.scrollView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 44);
	}
}

- (void)viewWillLayoutSubviews {
	CGSize bounds = self.view.bounds.size;
	int border = 10;
	int contentWidth = bounds.width - 4 * border;
	CGSize titleLabelSize = [self.titleLabel.text sizeWithFont:self.titleLabel.font
											 constrainedToSize:CGSizeMake(contentWidth, 25)
												 lineBreakMode:NSLineBreakByTruncatingTail];
	self.titleLabel.frame = CGRectMake(border,
									   border,
									   titleLabelSize.width,
									   titleLabelSize.height);
	
	CGSize ownerLabelSize = [self.ownerLabel.text sizeWithFont:self.ownerLabel.font
											 constrainedToSize:CGSizeMake(contentWidth, 20)
												 lineBreakMode:NSLineBreakByTruncatingTail];
	self.ownerLabel.frame = CGRectMake(border,
									   CGRectGetMaxY(self.titleLabel.frame) + border,
									   ownerLabelSize.width,
									   ownerLabelSize.height);
	
	CGSize dateLabelSize = [self.dateLabel.text sizeWithFont:self.dateLabel.font
										   constrainedToSize:CGSizeMake(contentWidth, 50)
											   lineBreakMode:NSLineBreakByTruncatingTail];
	self.dateLabel.frame = CGRectMake(border,
									  CGRectGetMaxY(self.ownerLabel.frame) + border,
									  dateLabelSize.width,
									  dateLabelSize.height);
	
	CGSize addressLabelSize = [self.addressLabel.text sizeWithFont:self.addressLabel.font
												 constrainedToSize:CGSizeMake(contentWidth, 100)];
	self.addressLabel.frame = CGRectMake(border,
										 CGRectGetMaxY(self.dateLabel.frame) + border,
										 addressLabelSize.width,
										 addressLabelSize.height);
	
	self.topImageView.frame = CGRectMake(border,
										 border,
										 bounds.width - 2 * border,
										 CGRectGetMaxY(self.addressLabel.frame) + border);
	
	CGSize descriptionViewSize = [self.descriptionTextView.text sizeWithFont:self.descriptionTextView.font
														   constrainedToSize:CGSizeMake(contentWidth, 150)];
	self.descriptionTextView.frame = CGRectMake(border,
												descriptionViewSize.height > 30 ? border: border + 4,
												bounds.width - 4 * border,
												descriptionViewSize.height);
	self.botImageView.frame = CGRectMake(border,
										 CGRectGetMaxY(self.topImageView.frame),
										 bounds.width - 2 * border,
										 MAX(CGRectGetMaxY(self.descriptionTextView.frame) + border, 46));
	
	CGSize mapSize = CGSizeMake(bounds.width - 2 * border, 150);
	 self.mapView.frame = CGRectMake(border,
	 CGRectGetMaxY(self.botImageView.frame) + border,
	 mapSize.width,
	 mapSize.height);
	 [self.mapView setRegion:MKCoordinateRegionMake(self.event.coordinate, MKCoordinateSpanMake(0.005, 0.005)) animated:NO];
	if (self.shareBar) {
		self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x,
										   self.scrollView.frame.origin.y,
										   self.view.frame.size.width,
										   self.view.frame.size.height - 44);
	}
	else {
		self.scrollView.frame = CGRectMake(self.scrollView.frame.origin.x,
										   self.scrollView.frame.origin.y,
										   self.view.frame.size.width,
										   self.view.frame.size.height);
	}
	self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width,
											 CGRectGetMaxY(self.mapView.frame) + border);
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.titleLabel.text = self.event.title;
	self.ownerLabel.text = [NSString stringWithFormat:@"Hosted by: %@", self.event.owner_username];
	self.addressLabel.text = [NSString stringWithFormat:@"At %@", self.event.address];
	self.descriptionTextView.text = self.event.event_description;
	[self viewWillLayoutSubviews];
}

- (void)mapViewWasClicked {
	MapViewController* controller = [[MapViewController alloc] init];
	controller.event = self.event;
	[self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
	return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
	return YES;
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id < MKAnnotation >)annotation {
	MKPinAnnotationView* pin = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:kPinIdentifier];
	if (!pin) {
		pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:kPinIdentifier];
	}
	pin.canShowCallout = NO;
	return pin;
}

#pragma mark - Handlers

- (void)userDidLeaveEvent {
	NSMutableSet* mutableSet = [NSMutableSet setWithSet:self.event.participants];
	[mutableSet removeObject:self.user];
	self.event.participants = mutableSet;
	UIBarButtonItem* button = self.navigationItem.rightBarButtonItem;
	button.title = @"Join";
	button.action = @selector(userDidJoinEvent);
	[[CoreDataManager sharedManager] saveToDiskAsynchronously:YES];
	if (self.shareBar) {
		[UIView animateWithDuration:0.3 animations:^{
			CGRect shareBarFrame = self.shareBar.frame;
			shareBarFrame.origin = CGPointMake(0, -44);
			self.shareBar.frame = shareBarFrame;
			self.scrollView.frame = CGRectMake(0,
											   0,
											   self.view.frame.size.width,
											   self.view.frame.size.height);
		} completion:^(BOOL finished) {
			if (finished) {
				[self.shareBar removeFromSuperview];
				self.shareBar = nil;
			}
		}];
	}
	[self deleteFromICal];
}

- (void)userDidJoinEvent {
	NSMutableSet* mutableSet = [NSMutableSet setWithSet:self.event.participants];
	[mutableSet addObject:self.user];
	self.event.participants = mutableSet;
	UIBarButtonItem* button = self.navigationItem.rightBarButtonItem;
	button.title = @"Leave";
	button.action = @selector(userDidLeaveEvent);
	[[CoreDataManager sharedManager] saveToDiskAsynchronously:YES];
	
	self.shareBar = [[UIView alloc] initWithFrame:CGRectMake(0,
															 -44,
															 self.view.frame.size.width,
															 44)];
	UIButton* shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
	UIImage* shareImage = [UIImage imageNamed:@"share-button.png"];
	[shareButton setImage:shareImage forState:UIControlStateNormal];
	shareButton.frame = CGRectMake(self.view.frame.size.width - shareImage.size.width - 5,
								   7,
								   shareImage.size.width,
								   shareImage.size.height);
	[shareButton addTarget:self action:@selector(loadSocialView) forControlEvents:UIControlEventTouchUpInside];
	[self.shareBar addSubview:shareButton];
	self.shareBar.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
	UIButton* icalButton = [UIButton buttonWithType:UIButtonTypeCustom];
	UIImage* icalImage = [UIImage imageNamed:@"ical-button.png"];
	[icalButton setImage:icalImage forState:UIControlStateNormal];
	icalButton.frame = CGRectMake(self.view.frame.size.width - 2 * shareImage.size.width - 2* 5,
								  7,
								  icalImage.size.width,
								  icalImage.size.height);
	[icalButton addTarget:self action:@selector(saveToICal) forControlEvents:UIControlEventTouchUpInside];
	self.iCalButton = icalButton;
	[self.shareBar addSubview:icalButton];
	[self.view addSubview:self.shareBar];
	
	
	
	[UIView animateWithDuration:0.3 animations:^{
		CGRect shareBarFrame = self.shareBar.frame;
		shareBarFrame.origin = CGPointZero;
		self.shareBar.frame = shareBarFrame;
		self.scrollView.frame = CGRectMake(0,
										   44,
										   self.view.frame.size.width,
										   self.view.frame.size.height - 44);
	}];
}

- (void)editButtonWasClicked {
	Reachability* reachability = [Reachability reachabilityForInternetConnection];
	if (reachability.currentReachabilityStatus == NotReachable) {
		UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"No internet connection"
															message:@"Sorry you can't edit any event while you're offline"
														   delegate:nil
												  cancelButtonTitle:@"Ok"
												  otherButtonTitles:nil];
		[alertView show];
		return;
	}
	AddEventViewController* controller = [[AddEventViewController alloc] initWithStyle:UITableViewStyleGrouped];
	controller.event = self.event;
	controller.user = self.user;
	controller.delegate = self;
	UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:controller];
	[self presentViewController:navController animated:YES completion:NULL];
}

- (void)loadSocialView {
	NSArray *activityItems;
	FileManager* fileManager = [FileManager sharedManager];
    UIImage *image  = [fileManager.cache objectForKey:self.event.image_identifier];
    if (!image) {
		image = [UIImage imageWithData:[fileManager dataFromDiskWithIdentifier:self.event.image_identifier.stringValue]];
		if (!image) {
			image = [UIImage imageNamed:@"no-image.png"];
		}
	}
    
	activityItems = @[[NSString stringWithFormat:@"I'm going to %@. Join too ! :)", self.event.title], image];
    
    UIActivityViewController *activityController =
    [[UIActivityViewController alloc]
     initWithActivityItems:activityItems
     applicationActivities:nil];
	activityController.excludedActivityTypes = [NSArray arrayWithObjects:UIActivityTypePostToWeibo,
												UIActivityTypeMail,
												UIActivityTypeCopyToPasteboard,
												UIActivityTypeAssignToContact,
												UIActivityTypeSaveToCameraRoll,
												UIActivityTypePrint, nil];
    
    [self presentViewController:activityController
                       animated:YES completion:nil];
}

- (void)deleteFromICal {
	EKEventStore *eventStore = [[EKEventStore alloc] init];
    if ([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)])
    {
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
				EKEvent* event = [eventStore eventWithIdentifier:self.event.iCalIdentifier];
				if (event) {
					self.event.iCalIdentifier = nil;
					CoreDataManager* manager = [CoreDataManager sharedManager];
					[manager saveToDiskAsynchronously:YES];
					[eventStore removeEvent:event span:EKSpanThisEvent commit:YES error:NULL];
				}
			});
		}];
		
	}
}

- (void)saveToICal {
	EKEventStore *eventStore = [[EKEventStore alloc] init];
    if ([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)])
    {
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error)
                {
                    NSLog(@"An error occured %@",error);
                }
                else if (!granted)
                {
                    NSLog(@"The user denied the access to iCal");
                }
                else
                {
                    
                    // Dealing with the Calendar
                    
                    BOOL calendarExists = NO ;
                    NSString *calendarIdentifier ; // Keep track of the A! events calendar
                    
                    for (EKCalendar *calendar in [eventStore calendarsForEntityType:EKEntityTypeEvent]){
                        
                        if ([calendar.title  isEqualToString:@"A! events"]){
                            
                            calendarExists = YES ;
                            calendarIdentifier = calendar.calendarIdentifier ;
                            break;
                        }
                    }
                    
                    if(!calendarExists){
                        
                        // Create the A! events calendar
                        
                        // find local source
                        
                        EKSource *localSource = nil;
                        for (EKSource *source in eventStore.sources)
                            if (source.sourceType == EKSourceTypeCalDAV && [source.title isEqualToString:@"iCloud"])
                            {
                                localSource = source;
                                break;
                            }
                        
                        
                        EKCalendar *cal = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:eventStore];
                        
                        // Set the paremeters of the calendar
                        
                        cal.title = @"A! events";
                        cal.source = localSource ;
                        
                        UIColor *color = [UIColor orangeColor] ;
                        CGColorRef colorref = [color CGColor];
                        cal.CGColor = colorref ;
                        
                        
                        NSError *error = nil;
                        [eventStore saveCalendar:cal commit:YES error:&error];
                        
                        if (!error) {
                            calendarIdentifier = cal.calendarIdentifier ;
                        } else {
							UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Sorry, we couldn't save the event in iCal" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
							[alert show];
                        }
                    }
					
					
					// Dealing with the events
                    
                    EKEvent *event  = [EKEvent eventWithEventStore:eventStore];
                    event.title = self.event.title; // title
                    
                    event.notes = self.event.event_description; // description
                    event.location = self.event.address ; // location
                    
					
                    
                    NSDateFormatter *tempFormatter = [[NSDateFormatter alloc]init];
                    [tempFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
                    
                    
                    event.startDate = self.event.date; 
                    event.endDate = [self.event.date dateByAddingTimeInterval: 3 * 60 * 60];
                    
                    
                    [event addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -60.0f * 24]]; // Set the alarm one day before
                    [event addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -60.0f]]; // Set the alarm one hour before
                    // Set the event on A! events calendar
                    [event setCalendar:[eventStore calendarWithIdentifier:calendarIdentifier]];
                    NSError *error;
                    [eventStore saveEvent:event span:EKSpanThisEvent error:&error];
					if (!error) {
						UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"The event was successfully saved in iCal" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
						[alert show];
						if (self.iCalButton) {
							self.iCalButton.enabled = NO;
						}
					}
					else {
						UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Sorry, we couldn't save the event in iCal" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
						[alert show];
					}
					self.event.iCalIdentifier = event.eventIdentifier;
					CoreDataManager* manager = [CoreDataManager sharedManager];
					[manager saveToDiskAsynchronously:YES];
                }
            });
        }];
	}
}


#pragma mark - AddEventViewControllerDelegate

- (void)addEventControllerDidDelete:(AddEventViewController *)controller {
	[self.navigationController popViewControllerAnimated:NO];
}

@end
