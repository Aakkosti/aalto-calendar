//
//  DatePickerViewController.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 2/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "DatePickerViewController.h"

@interface DatePickerViewController ()
@property (strong, readwrite) UIDatePicker* datePicker;
@end

@implementation DatePickerViewController

- (id)init {
	self = [super init];
	if (self) {
		self.title = @"Choose date";
	}
	return self;
}

- (UINavigationItem*)navigationItem {
	UINavigationItem* item = [super navigationItem];
	item.backBarButtonItem = nil;
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]
									 initWithTitle:@"Cancel"
									 style:UIBarButtonItemStyleBordered
									 target:self
									 action:@selector(cancelButtonWasClicked)];
	item.leftBarButtonItem = cancelButton;
	UIBarButtonItem* doneButton = [[UIBarButtonItem alloc]
									 initWithTitle:@"Done"
									 style:UIBarButtonItemStyleBordered
									 target:self
									 action:@selector(doneButtonWasClicked)];
	item.rightBarButtonItem = doneButton;
	return item;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor colorWithRed:0.9135 green:0.9135 blue:0.9135 alpha:1.0000];
	self.datePicker = [[UIDatePicker alloc] init];
	self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
	self.datePicker.minimumDate = [NSDate date];
	self.datePicker.maximumDate = [NSDate dateWithTimeIntervalSinceNow:3600*24*365];
	if (self.startDate) {
		self.datePicker.date = self.startDate;
	}
	[self.datePicker sizeToFit];
	[self.view addSubview:self.datePicker];
}

- (void)viewWillLayoutSubviews {
	CGRect frame = self.datePicker.frame;
	frame.origin = CGPointMake(0, 0);
	self.datePicker.frame = frame;
}

#pragma mark - Handlers

- (void)cancelButtonWasClicked {
	if (self.delegate && [self.delegate respondsToSelector:@selector(datePickerDidClickCancel:)]) {
		[self.delegate datePickerDidClickCancel:self];
	}
}

- (void)doneButtonWasClicked {
	if (self.delegate && [self.delegate respondsToSelector:@selector(datePicker:didClickDoneWithDate:)]) {
		[self.delegate datePicker:self didClickDoneWithDate:self.datePicker.date];
	}
}


@end
