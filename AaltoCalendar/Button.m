//
//  Button.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 1/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "Button.h"

@implementation Button

- (id)initWithFrame:(CGRect)frame {
	self = [UIButton buttonWithType:UIButtonTypeCustom];
	if (self) {
		UIImage* image = [[UIImage imageNamed:@"button.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:0];
		[self setBackgroundImage:image forState:UIControlStateNormal];
		[self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
		[self setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[self setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
		[self setTitleShadowColor:[UIColor blackColor] forState:UIControlStateHighlighted];
		self.titleLabel.shadowOffset = CGSizeMake(0, 1);
		self.frame = CGRectMake(frame.origin.x,
								frame.origin.y,
								frame.size.width,
								44);
	}
	return self;;
}



@end
