//
//  TextField.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 1/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	TextFieldFull = 0,
	TextFieldTop = 1,
	TextFieldMid = 2,
	TextFieldBot = 3
} TextFieldStyle;

@interface TextField : UITextField
- (id)initWithStyle:(TextFieldStyle)style;
@end
