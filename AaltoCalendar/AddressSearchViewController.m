//
//  AddressSearchViewController.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 2/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "AddressSearchViewController.h"
#import "JsonRequest.h"

#define kCellIdentifier @"Cell"
#define kGoogleAPI @"http://maps.googleapis.com/maps/api/geocode/json"

@interface AddressSearchViewController ()
@property (strong, readwrite) UISearchDisplayController* searchController;
@property (strong, readwrite) NSMutableArray* addresses;
@property (strong, readwrite) NSMutableArray* locations;
@end

@implementation AddressSearchViewController

- (id)init {
	self = [super init];
	if (self) {
		UISearchBar* searchBar = [[UISearchBar alloc] initWithFrame:CGRectZero];
		searchBar.delegate = self;
		[searchBar sizeToFit];
		UISearchDisplayController* searchController = [[UISearchDisplayController alloc] initWithSearchBar:searchBar
															  contentsController:self];
		searchController.searchResultsDataSource = self;
		searchController.searchResultsDelegate = self;
		searchController.delegate = self;
		_searchController = searchController;
		self.title = @"Find address";
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.view addSubview:self.searchDisplayController.searchBar];
}

- (void)viewWillLayoutSubviews {
	CGRect frame = self.searchDisplayController.searchBar.frame;
	frame.origin = CGPointZero;
}

- (UINavigationItem*)navigationItem {
	UINavigationItem* item = [super navigationItem];
	item.backBarButtonItem = nil;
	UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc]
									 initWithTitle:@"Cancel"
									 style:UIBarButtonItemStyleBordered
									 target:self
									 action:@selector(cancelButtonWasClicked)];
	item.leftBarButtonItem = cancelButton;
	return item;
}

#pragma mark - UISearchDisplayDelegate

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller {
	self.addresses = nil;
	self.locations = nil;
}

- (BOOL)searchDisplayController:(UISearchDisplayController*)controller shouldReloadTableForSearchString:(NSString*)searchString {
	return NO;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
	[tableView registerClass:UITableViewCell.class forCellReuseIdentifier:kCellIdentifier];
}


#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	if (searchText.length == 0) {
		self.addresses = nil;
		self.locations = nil;
		[self.searchDisplayController.searchResultsTableView reloadData];
	}
	else if (searchText.length > 2) {
		NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?address=%@&sensor=false&components=country:FI",
										   kGoogleAPI,
										   [searchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
		JsonRequest* request = [[JsonRequest alloc] init];
		request.url = url;
		request.method = httpMethodGet;
		[request fetch:^(NSObject *json) {
			self.addresses = [[NSMutableArray alloc] init];
			self.locations = [[NSMutableArray alloc] init];
			if (!json) {
				return;
			}
			NSDictionary* jsonDic = (NSDictionary*)json;
			if (![[jsonDic objectForKey:@"status"] isEqualToString:@"OK"]) {
				return;
			}
			NSArray* results = [jsonDic objectForKey:@"results"];
			for (NSDictionary* result in results) {
				for (NSDictionary* component in [result objectForKey:@"address_components"]) {
					NSArray* types = [component objectForKey:@"types"];
					if ([types containsObject:@"route"] || [types containsObject:@"street"]) {
						NSString* address = [result objectForKey:@"formatted_address"];
						if (![address isEqualToString:@"Finland"]) {
							NSDictionary* locations = [[result objectForKey:@"geometry"] objectForKey:@"location"];
							NSString* location = [NSString stringWithFormat:@"(%@,%@)",
												  [locations objectForKey:@"lat"],
												  [locations objectForKey:@"lng"]];
							[self.locations addObject:location];
							[self.addresses addObject:address];
							break;
						}
					}
				}
			}
			[self.searchDisplayController.searchResultsTableView reloadData];
		}];
	}
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.addresses.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	cell.textLabel.text = [self.addresses objectAtIndex:indexPath.row];
	return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (self.delegate && [self.delegate respondsToSelector:@selector(addressController:didSelectAddress:withLocation:)]) {
		NSString* address = [self.addresses objectAtIndex:indexPath.row];
		NSString* location = [self.locations objectAtIndex:indexPath.row];
		[self.delegate addressController:self
						didSelectAddress:address
							withLocation:location];
	}
}

#pragma mark - Handlers

- (void)cancelButtonWasClicked {
	if (self.delegate && [self.delegate respondsToSelector:@selector(addressControllerDidClickCancel:)]) {
		[self.delegate addressControllerDidClickCancel:self];
	}
}

@end
