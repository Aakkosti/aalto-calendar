//
//  DeletedEvent.h
//  AaltoCalendar
//
//  Created by Jaakko Kaisanlahti on 2013/05/01.
//  Copyright (c) 2013年 Group 14. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DeletedEvent : NSManagedObject

@property (nonatomic, retain) NSNumber * identifier;

@end
