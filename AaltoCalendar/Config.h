//
//  Config.h
//  AaltoCalendar
//
//  Created by Jaakko Kaisanlahti on 2013/03/22.
//  Copyright (c) 2013年 Group 14. All rights reserved.
//

#ifndef AaltoCalendar_Config_h
#define AaltoCalendar_Config_h

#define SERVER_URL @"https://aalto-calendar.herokuapp.com/"
#define KEYCHAIN_SERVICE @"aaltoCalendar"



//JSON keys
#define EVENT_ID @"id"
#define EVENT_TITLE @"title"
#define EVENT_DATE @"date"
#define EVENT_DESCRIPTION @"description"
#define EVENT_LOCATION @"location"
#define EVENT_ADDRESS @"address"
#define EVENT_IMAGE_URL @"image_url"
#define EVENT_USER_ID @"application_user"
#define EVENT_OWNER_USERNAME @"owner_username"
#define EVENT_IMAGE_ID @"image_identifier"


//Notifications
#define DID_START_SYNCHRONIZING @"did_start_synchronizing"
#define DID_FINISH_SYNCHRONIZING @"did_finish_synchronizing"
#define DID_FAIL_SYNCHRONIZING @"did_fail_synchronizing"

#endif
