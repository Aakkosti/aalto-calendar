//
//  SignInViewController.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 26/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "JsonRequest.h"
#import "Config.h"
#import "CoreDataManager.h"
#import "SFHFKeychainUtils.h"
#import "TextField.h"
#import "Button.h"	

@interface SignInViewController ()
@property (strong, readwrite) TextField* usernameField;
@property (strong, readwrite) TextField* passwordField;
@property (strong, readwrite) Button* signInButton;
@end

@implementation SignInViewController

- (UINavigationItem*)navigationItem {
	UINavigationItem* item = [super navigationItem];
	item.hidesBackButton = YES;
	item.title = @"Sign in";
	UIBarButtonItem* buttonItem = [[UIBarButtonItem alloc] initWithTitle:@"Sign up"
																   style:UIBarButtonItemStyleBordered
																  target:self
																  action:@selector(didClickSignUpBarButton)];
	item.rightBarButtonItem = buttonItem;
	return item;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.usernameField = [[TextField alloc] initWithStyle:TextFieldTop];
	self.usernameField.placeholder = @"Username";
	self.usernameField.delegate = self;
	[self.view addSubview:self.usernameField];
	
	self.passwordField = [[TextField alloc] initWithStyle:TextFieldBot];
	self.passwordField.placeholder = @"Password";
	self.passwordField.secureTextEntry = YES;
	self.passwordField.delegate = self;
	[self.view addSubview:_passwordField];
	
	self.signInButton = [[Button alloc] init];
	[self.signInButton setTitle:@"Sign in" forState:UIControlStateNormal];
	[self.view addSubview:self.signInButton];
	[self.signInButton addTarget:self
						  action:@selector(didClickSignInButton)
				forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillLayoutSubviews {
	CGSize textFieldSize = CGSizeMake(300, 44);
	CGSize boundsSize = self.view.bounds.size;
	self.usernameField.frame = CGRectMake((boundsSize.width - textFieldSize.width)/2,
										  boundsSize.height/2 - 2*textFieldSize.height,
										  textFieldSize.width,
										  textFieldSize.height);
	self.passwordField.frame = CGRectMake((boundsSize.width - textFieldSize.width)/2,
										  CGRectGetMaxY(_usernameField.frame),
										  textFieldSize.width,
										  textFieldSize.height);
	CGSize buttonSize = CGSizeMake(100, 44);
	self.signInButton.frame = CGRectMake((boundsSize.width - buttonSize.width)/2,
																 CGRectGetMaxY(_passwordField.frame) + 10,
																 buttonSize.width,
																 buttonSize.height);
}

#pragma mark - Handlers

- (void)didClickSignUpBarButton {
	UINavigationController* navigationController = self.navigationController;
	if (navigationController.viewControllers.count > 1)
		[navigationController popViewControllerAnimated:NO];
	else {
		SignUpViewController* viewController = [[SignUpViewController alloc] init];
		viewController.delegate = self.delegate;
		[self.navigationController pushViewController:viewController animated:NO];
	}
}

- (void)didClickSignInButton {
	self.signInButton.enabled = NO;
	NSString* username = self.usernameField.text;
	NSData* usernameData = [username dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
	username = [[NSString alloc] initWithData:usernameData encoding:NSUTF8StringEncoding];
	NSString* password = self.passwordField.text;
	NSData* passwordData = [password dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
	password = [[NSString alloc] initWithData:passwordData encoding:NSUTF8StringEncoding];
	NSURLCredential* credentials = [[NSURLCredential alloc] initWithUser:username
																password:password
															 persistence:NSURLCredentialPersistenceNone];
	JsonRequest* request = [[JsonRequest alloc] init];
	request.url = [NSURL URLWithString:@"user" relativeToURL:[NSURL URLWithString:SERVER_URL]];
	request.method = httpMethodGet;
	request.credentials = credentials;
	
	[request fetch:^(NSObject *json) {
		if (!json) {
			UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Oups"
																message:@"Something went wrong. Please try again"
															   delegate:self
													  cancelButtonTitle:nil
													  otherButtonTitles:@"Ok", nil];
			[alertView show];
			self.signInButton.enabled = YES;
			return;
		}
		NSDictionary* jsonDict = (NSDictionary*)json;
		if ([jsonDict objectForKey:@"success"]) {
			NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:@"User"];
			request.predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"username", username];
			CoreDataManager* manager = [CoreDataManager sharedManager];
			NSError* error = nil;
			NSArray* array = [manager.moc executeFetchRequest:request
														error:&error];
			if (error) {
				UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Oups"
																	message:@"Something went wrong. Please try again"
																   delegate:self
														  cancelButtonTitle:nil
														  otherButtonTitles:@"Ok", nil];
				[alertView show];
				self.signInButton.enabled = YES;
				return;
			}
			User* user;
			if (array.count == 0) {
				user = [NSEntityDescription insertNewObjectForEntityForName:@"User"
													 inManagedObjectContext:manager.moc];
				user.username = username;
			}
			else {
				user = [array objectAtIndex:0];
			}
			user.identifier = [jsonDict objectForKey:@"id"];
			user.isCurrentUser = [NSNumber numberWithBool:YES];
			[manager saveToDiskAsynchronously:YES];
			
			[SFHFKeychainUtils storeUsername:username
								 andPassword:password
							  forServiceName:KEYCHAIN_SERVICE
							  updateExisting:YES
									   error:&error];
			if (error) {
				NSLog(@"couldn't store in keychain during login: %@", error);
				self.signInButton.enabled = YES;
				return;
			}
			if (self.delegate && [self.delegate respondsToSelector:@selector(signInViewController:didSignInUser:)]) {
				[self.delegate signInViewController:self didSignInUser:user];
			}
		}
		else if ([[jsonDict objectForKey:@"error"] isEqualToString:@"incorrect username/password"]){
			UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Incorrect username/password"
																message:@"Username and password don't match. Please try again."
															   delegate:self
													  cancelButtonTitle:nil
													  otherButtonTitles:@"Ok", nil];
			[alertView show];
			self.signInButton.enabled = YES;
			return;
		}
		self.signInButton.enabled = YES;
	}];
}

#pragma mark - AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	[alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}

@end
