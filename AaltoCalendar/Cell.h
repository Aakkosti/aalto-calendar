//
//  Cell.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 29/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

#define kCellHeight 90

@interface Cell : UITableViewCell
@property (strong, readwrite) Event* event;
@end
