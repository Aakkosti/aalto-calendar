//
//  AaltoCalendarAppDelegate.h
//  AaltoCalendar
//
//  Created by Jaakko Kaisanlahti on 2013/03/22.
//  Copyright (c) 2013年 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Synchronizer.h"


@interface AaltoCalendarAppDelegate : UIResponder <UIApplicationDelegate, SynchronizerDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
