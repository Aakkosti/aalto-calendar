//
//  MyEventsViewController.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 26/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "MyEventsViewController.h"
#import "Config.h"
#import "Synchronizer.h"
#import "CoreDataManager.h"
#import "Event.h"
#import "Cell.h"
#import "EventDetailViewController.h"
#import "AddEventViewController.h"
#import "Reachability.h"

#define kCellIdentifier @"Cell"

@interface MyEventsViewController ()
@property (strong, readwrite) NSFetchedResultsController* fetchController;
@end

@implementation MyEventsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
		CoreDataManager* manager =  [CoreDataManager sharedManager];
		NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:@"Event"];
		NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
		NSSortDescriptor* sortDescriptor2 = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
		request.sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, sortDescriptor2, nil];
		_fetchController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
															   managedObjectContext:manager.moc
																 sectionNameKeyPath:nil
																		  cacheName:nil];
		_fetchController.delegate = self;
		request.fetchBatchSize = 15;
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(synchronizerDidFinishSyncing)
													 name:DID_FINISH_SYNCHRONIZING object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(synchronizerDidFinishSyncing)
													 name:DID_FAIL_SYNCHRONIZING object:nil];
		self.title = @"Mine";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	NSError* error = nil;
	UIRefreshControl* refreshControl = [[UIRefreshControl alloc] init];
	[refreshControl addTarget:[Synchronizer sharedSynchronizer] action:@selector(synchronize) forControlEvents:UIControlEventValueChanged];
	refreshControl.tintColor = [UIColor blackColor];
	self.refreshControl = refreshControl;
	NSDate* fiveHoursAgo = [NSDate dateWithTimeIntervalSinceNow:(-5*3600)];
	NSPredicate* predicate = [NSPredicate predicateWithFormat:@"%K == %@ && %K > %@ ", @"owner", self.user, @"date", fiveHoursAgo];
	self.fetchController.fetchRequest.predicate = predicate;
	[self.fetchController performFetch:&error];
	if (error) {
		NSLog(@"couldn't fetch events:%@", error);
	}
	self.tableView.allowsSelectionDuringEditing = YES;
	[self.tableView registerClass:Cell.class forCellReuseIdentifier:kCellIdentifier];
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UINavigationItem*)navigationItem {
	UINavigationItem* item = [super navigationItem];
	UIBarButtonItem* addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
																			   target:self
																			   action:@selector(addButtonWasClicked)];
	item.rightBarButtonItem = addButton;
	return item;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchController.sections.count;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.fetchController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	Cell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
	cell.event = [self.fetchController.fetchedObjects objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return kCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	EventDetailViewController* controller = [[EventDetailViewController alloc] init];
	controller.user = self.user;
	controller.event = [self.fetchController.fetchedObjects objectAtIndex:indexPath.row];
	[self.navigationController pushViewController:controller animated:YES];
	controller.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
																								 target:controller
																								 action:@selector(editButtonWasClicked)];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

#pragma mark - Handlers

- (void)synchronizerDidFinishSyncing {
	[self.refreshControl endRefreshing];
}

- (void)addButtonWasClicked {
	Reachability* reachability = [Reachability reachabilityForInternetConnection];
	if (reachability.currentReachabilityStatus == NotReachable) {
		UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"No internet connection"
															message:@"Sorry you can't add any event while you're offline"
														   delegate:nil
												  cancelButtonTitle:@"Ok"
												  otherButtonTitles:nil];
		[alertView show];
		return;
	}
	AddEventViewController* controller = [[AddEventViewController alloc] initWithStyle:UITableViewStyleGrouped];
	UINavigationController* navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
	controller.user = self.user;
	[self presentViewController:navigationController animated:YES completion:NULL];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	[self.tableView reloadData];
}


@end
