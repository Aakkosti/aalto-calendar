//
//  CoreDataManager.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 24/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "CoreDataManager.h"

static CoreDataManager* sharedManager = nil;

@interface CoreDataManager ()
@property (strong, readonly) NSManagedObjectModel* managedObjectModel;
@property (strong, readonly) NSPersistentStoreCoordinator* persistentStoreCoordinator;
@end

@implementation CoreDataManager
@synthesize moc = _moc,
			backgroundMoc = _backgroundMoc,
			managedObjectModel = _managedObjectModel,
			persistentStoreCoordinator = _persistentStoreCoordinator;

+ (CoreDataManager*)sharedManager {
	if (!sharedManager) {
		sharedManager = [[CoreDataManager alloc] init];
	}
	return sharedManager;
}

- (void)saveToDiskAsynchronously:(BOOL)asynchronously {
	if (self.moc.hasChanges) {
		[self.moc performBlockAndWait:^{
			NSError* error = nil;
			[self.moc save:&error];
			if (error) {
				NSLog(@"main moc couldn't be saved : %@", error);
				return;
			}
		}];
	}
	if (self.backgroundMoc) {
		void (^block)(void) = ^{
			NSError* error = nil;
			[self.backgroundMoc save:&error];
			if (error) {
				NSLog(@"background moc couldn't be savec: %@", error);
			}
		};
		if (asynchronously) {
			[self.backgroundMoc performBlock:block];
		}
		else {
			[self.backgroundMoc performBlockAndWait:block];
		}
	}
}

#pragma mark - Core Data stack

// Returns the main managed object context for the application.
- (NSManagedObjectContext *)moc {
    if (!_moc) {
		_moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
		_moc.undoManager = nil;
		_moc.parentContext = self.backgroundMoc;
	}
    return _moc;
}

// Returns the background managed object context for the application.
- (NSManagedObjectContext*)backgroundMoc {
	if (!_backgroundMoc) {
		NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
		if (coordinator != nil) {
			_backgroundMoc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
			_backgroundMoc.undoManager = nil;
			_backgroundMoc.persistentStoreCoordinator = coordinator;
		}
	}
    return _backgroundMoc;

}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (!_managedObjectModel) {
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"AaltoCalendar" withExtension:@"momd"];
		_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (!_persistentStoreCoordinator) {
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"AaltoCalendar.sqlite"];
		
		NSError *error = nil;
		_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
		if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
			/*
			 Replace this implementation with code to handle the error appropriately.
			 
			 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			 
			 Typical reasons for an error here include:
			 * The persistent store is not accessible;
			 * The schema for the persistent store is incompatible with current managed object model.
			 Check the error message to determine what the actual problem was.
			 
			 
			 If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
			 
			 If you encounter schema incompatibility errors during development, you can reduce their frequency by:
			 * Simply deleting the existing store:
			 [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
			 
			 * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
			 @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
			 
			 Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
			 
			 */
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			abort();
		}
    }
	return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
