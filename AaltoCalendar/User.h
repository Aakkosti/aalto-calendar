//
//  User.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 30/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Event;

@interface User : NSManagedObject

@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSNumber * isCurrentUser;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *events;
@property (nonatomic, retain) NSSet *toGoEvents;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addEventsObject:(Event *)value;
- (void)removeEventsObject:(Event *)value;
- (void)addEvents:(NSSet *)values;
- (void)removeEvents:(NSSet *)values;

- (void)addToGoEventsObject:(Event *)value;
- (void)removeToGoEventsObject:(Event *)value;
- (void)addToGoEvents:(NSSet *)values;
- (void)removeToGoEvents:(NSSet *)values;

@end
