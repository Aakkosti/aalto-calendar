//
//  MapViewController.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 30/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Event+Util.h"

@interface MapViewController : UIViewController
@property (strong, readwrite) Event* event;
@end
