//
//  JsonRequest.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 19/03/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "JsonRequest.h"

@interface JsonRequest ()
@property (strong, readwrite) NSMutableData* mutableData;
@end

@implementation JsonRequest
@synthesize url = _url,
			body = _body,
			method = _method,
			credentials = _credentials;

- (NSString *)serializeJson:(NSObject *)json {
	NSError* error;
	NSData* data = [NSJSONSerialization dataWithJSONObject:json options:0 error:&error];
	if (!data)
		return nil;
	return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

- (void)fetch:(void (^)(NSObject * json))callbackBlock {
	DataRequest* request = [[DataRequest alloc] init];
	request.url = self.url;
	if (self.body) {
		NSString* jsonBody = [self serializeJson:self.body];
		if (!jsonBody) {
			callbackBlock(nil);
			return;
		}
		request.body = [jsonBody dataUsingEncoding:NSUTF8StringEncoding];
	}
	request.method = self.method;
	request.credentials = self.credentials;
	[request fetch:^(NSData *data) {
		if (!data) {
			callbackBlock(nil);
			return;
		}
		NSObject *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
		if (json) {
			dispatch_async(dispatch_get_main_queue(), ^{
				callbackBlock(json);
			});
		}
		else {
			callbackBlock(nil);
		}

	}];
}
@end
