//
//  SignUpViewController.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 25/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
@class SignUpViewController;

@protocol SignUpViewControllerDelegate <NSObject>
- (void)signUpViewController:(SignUpViewController*)controller didSignUpUser:(User*)user;
@end

@interface SignUpViewController : UIViewController <UIAlertViewDelegate, UITextFieldDelegate>
@property (weak, readwrite) id delegate;
@end
