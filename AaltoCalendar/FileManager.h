//
//  FileManager.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 2/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject
@property (strong, readonly) NSCache* cache;
+ (FileManager*)sharedManager;
- (BOOL)writeDataToDisk:(NSData*)data withIdentifier:(NSString*)identifier;
- (NSData*)dataFromDiskWithIdentifier:(NSString *)identifier;
@end
