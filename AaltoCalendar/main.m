//
//  main.m
//  AaltoCalendar
//
//  Created by Jaakko Kaisanlahti on 2013/03/22.
//  Copyright (c) 2013年 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AaltoCalendarAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AaltoCalendarAppDelegate class]));
    }
}
