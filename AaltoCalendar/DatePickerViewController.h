//
//  DatePickerViewController.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 2/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DatePickerViewController;

@protocol DatePickerViewControllerDelegate <NSObject>
- (void)datePicker:(DatePickerViewController*)controller didClickDoneWithDate:(NSDate*)date;
- (void)datePickerDidClickCancel:(DatePickerViewController *)controller;

@end

@interface DatePickerViewController : UIViewController
@property (weak, readwrite) id delegate;
@property (strong, readwrite) NSDate* startDate;
@end
