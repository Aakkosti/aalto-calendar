//
//  LoadingView.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 26/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "LoadingView.h"

@interface LoadingView ()
@property (strong, readwrite) UIActivityIndicatorView* activityIndicator;
@end

@implementation LoadingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
		CGSize labelSize = CGSizeMake(200, 50);
		CGSize boundsSize = self.bounds.size;
		_label = [[UILabel alloc] initWithFrame:CGRectMake((boundsSize.width - labelSize.width) / 2,
														   200,
														   labelSize.width,
														   labelSize.height)];
		_label.textColor = [UIColor blackColor];
		_label.textAlignment = NSTextAlignmentCenter;
		_label.lineBreakMode = NSLineBreakByTruncatingTail;
		[self addSubview:_label];
		
		_activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		[_activityIndicator sizeToFit];
		CGRect activityIndicatorFrame = _activityIndicator.frame;
		activityIndicatorFrame.origin = CGPointMake((boundsSize.width - activityIndicatorFrame.size.width) / 2,
													CGRectGetMaxY(_label.frame) + 25);
		_activityIndicator.frame = activityIndicatorFrame;
		[self addSubview:_activityIndicator];
		[_activityIndicator startAnimating];
    }
    return self;
}


@end
