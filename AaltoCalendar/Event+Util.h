//
//  Event+Util.h
//  AaltoCalendar
//
//  Created by Jaakko Kaisanlahti on 2013/03/22.
//  Copyright (c) 2013年 Group 14. All rights reserved.
//

#import "Event.h"
#import <MapKit/MapKit.h>

@interface Event (Util) <MKAnnotation>
+ (Event *)eventWithDictionary:(NSDictionary *)dictionary fromContext:
    (NSManagedObjectContext *)context;
- (NSDictionary *)dictionary;
@end
