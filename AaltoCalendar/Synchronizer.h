//
//  Synchronizer.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 25/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "JsonRequest.h"
@class Synchronizer;

@protocol SynchronizerDelegate <NSObject>
- (void)synchronizerDidStartSyncing:(Synchronizer*)synchronizer;
- (void)synchronizerDidFinishSyncing:(Synchronizer*)synchronizer;
- (void)synchronizerDidFailSyncing:(Synchronizer*)synchronizer error:(NSError*)error;
@end

@interface Synchronizer : NSObject
@property (unsafe_unretained) id delegate;
@property (strong, nonatomic) User *user;
- (void)synchronize;
+ (Synchronizer *)sharedSynchronizer;
- (void)updateEvent:(Event*)event;
- (void)addEvent:(Event*)event;
- (void)deleteEvent:(Event *)event;
- (void)uploadImage:(NSData *)image withBlock:(void(^) (NSString *imageUrl))block;
@end
