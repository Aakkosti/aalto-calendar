//
//  SignUpViewController.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 25/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "SignUpViewController.h"
#import "JsonRequest.h"
#import "Config.h"
#import "SFHFKeychainUtils.h"
#import "CoreDataManager.h"
#import "SignInViewController.h"
#import "TextField.h"
#import "Button.h"

@interface SignUpViewController ()
@property (strong, readwrite) TextField* usernameField;
@property (strong, readwrite) TextField* passwordField;
@property (strong, readwrite) TextField* repeatPasswordField;
@property (strong, readwrite) Button* signUpButton;
@end

@implementation SignUpViewController

- (UINavigationItem*)navigationItem {
	UINavigationItem* item = [super navigationItem];
	item.hidesBackButton = YES;
	item.title = @"Sign up";
	UIBarButtonItem* buttonItem = [[UIBarButtonItem alloc] initWithTitle:@"Sign in"
																   style:UIBarButtonItemStyleBordered
																  target:self
																  action:@selector(didClickSignInBarButton)];
	item.rightBarButtonItem = buttonItem;
	return item;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.usernameField = [[TextField alloc] initWithStyle:TextFieldTop];
	self.usernameField.placeholder = @"Username";
	self.usernameField.delegate = self;
	[self.view addSubview:_usernameField];
	
	self.passwordField = [[TextField alloc] initWithStyle:TextFieldMid];
	self.passwordField.placeholder = @"Password";
	self.passwordField.secureTextEntry = YES;
	self.passwordField.delegate = self;
	[self.view addSubview:_passwordField];
	
	self.repeatPasswordField = [[TextField alloc] initWithStyle:TextFieldBot];
	self.repeatPasswordField.placeholder = @"Repeat password";
	self.repeatPasswordField.secureTextEntry = YES;
	self.repeatPasswordField.delegate = self;
	[self.view addSubview:_repeatPasswordField];
	self.signUpButton = [[Button alloc] init];
	[self.signUpButton setTitle:@"Sign up" forState:UIControlStateNormal];
	[self.signUpButton addTarget:self action:@selector(didClickSignUpButton) forControlEvents:UIControlEventTouchUpInside];
	
	
	[self.view addSubview:_signUpButton];
}

- (void)viewWillLayoutSubviews {
	CGSize textFieldSize = CGSizeMake(300, 44);
	CGSize boundsSize = self.view.bounds.size;
	self.usernameField.frame = CGRectMake((boundsSize.width - textFieldSize.width)/2,
										  boundsSize.height/2 - 2.5*textFieldSize.height,
										  textFieldSize.width,
										  textFieldSize.height);
	self.passwordField.frame = CGRectMake((boundsSize.width - textFieldSize.width)/2,
										  CGRectGetMaxY(_usernameField.frame),
										  textFieldSize.width,
										  textFieldSize.height);
	self.repeatPasswordField.frame = CGRectMake((boundsSize.width - textFieldSize.width)/2,
												CGRectGetMaxY(_passwordField.frame),
												textFieldSize.width,
												textFieldSize.height);
	CGSize buttonSize = CGSizeMake(100, 44);
	self.signUpButton.frame = CGRectMake((boundsSize.width - buttonSize.width)/2,
					 CGRectGetMaxY(_repeatPasswordField.frame) + 10,
					 buttonSize.width,
					 buttonSize.height);
}

#pragma mark - Handlers

- (void)didClickSignInBarButton {
	UINavigationController* navigationController = self.navigationController;
	if (navigationController.viewControllers.count > 1)
		[navigationController popViewControllerAnimated:NO];
	else {
		SignInViewController* viewController = [[SignInViewController alloc] init];
		viewController.delegate = self.delegate;
		[self.navigationController pushViewController:viewController animated:NO];
	}
}

- (void)didClickSignUpButton {
	self.signUpButton.enabled = NO;
	NSString* username = self.usernameField.text;
	NSData* usernameData = [username dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
	username = [[NSString alloc] initWithData:usernameData encoding:NSUTF8StringEncoding];
	NSString* password = self.passwordField.text;
	NSData* passwordData = [password dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
	password = [[NSString alloc] initWithData:passwordData encoding:NSUTF8StringEncoding];
	NSString* repeatedPassword = self.repeatPasswordField.text;
	NSData* repeatedPasswordData = [repeatedPassword dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
	repeatedPassword = [[NSString alloc] initWithData:repeatedPasswordData encoding:NSUTF8StringEncoding];

	if (![password isEqualToString:repeatedPassword]) {
		UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Passwords don't match"
															message:@"You mistyped your password. Please try again"
														   delegate:self
												  cancelButtonTitle:nil
												  otherButtonTitles:@"Ok", nil];
		[alertView show];
		self.signUpButton.enabled = YES;
		return;
	}
	
	JsonRequest* request = [[JsonRequest alloc] init];
	NSURL* baseUrl = [NSURL URLWithString:SERVER_URL];
	NSURL* url = [baseUrl URLByAppendingPathComponent:@"user"];
	request.url = url;
	request.method = httpMethodPut;
	request.body = [[NSDictionary alloc] initWithObjectsAndKeys:username, @"username", password, @"password", nil];
	[request fetch:^(NSObject *json) {
		NSLog(@"%@", json);
		if (!json) {
			UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Oups"
																message:@"Something went wrong. Please try again"
															   delegate:self
													  cancelButtonTitle:nil
													  otherButtonTitles:@"Ok", nil];
			[alertView show];
			self.signUpButton.enabled = YES;
			return;
		}
		NSDictionary* jsonDict = (NSDictionary*)json;
		if ([jsonDict objectForKey:@"success"]) {
			CoreDataManager* manager = [CoreDataManager sharedManager];
			User* user = [NSEntityDescription insertNewObjectForEntityForName:@"User"
													   inManagedObjectContext:manager.moc];
			user.username = username;
			user.identifier = [jsonDict objectForKey:@"id"];
			user.isCurrentUser = [NSNumber numberWithBool:YES];
			NSError* error = nil;
			[SFHFKeychainUtils storeUsername:username
								 andPassword:password
							  forServiceName:KEYCHAIN_SERVICE
							  
							  updateExisting:YES error:&error];
			if (error) {
				NSLog(@"couldn't store user in keychain: %@", error);
				self.signUpButton.enabled = YES;
				return;
			}
			[manager saveToDiskAsynchronously:YES];
			if (self.delegate && [self.delegate respondsToSelector:@selector(signUpViewController:didSignUpUser:)]) {
				[self.delegate signUpViewController:self didSignUpUser:user];
			}
		}
		else if ([[jsonDict objectForKey:@"error"] isEqualToString:@"username already in use"]) {
			UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Username unavailable"
																message:@"Username already in use. Please choose another one"
															   delegate:self
													  cancelButtonTitle:nil
													  otherButtonTitles:@"Ok", nil];
			[alertView show];
			self.signUpButton.enabled = YES;
			return;
		}
		self.signUpButton.enabled = YES;
	}];
}

#pragma mark - AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	[alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}


@end
