//
//  CoreDataManager.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 24/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataManager : NSObject
@property (strong, readonly) NSManagedObjectContext* moc;
@property (strong, readonly) NSManagedObjectContext* backgroundMoc;
+ (CoreDataManager*)sharedManager;
- (void)saveToDiskAsynchronously:(BOOL)asynchronously;
@end
