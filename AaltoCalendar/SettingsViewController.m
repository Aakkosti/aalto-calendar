//
//  SettingsViewController.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 26/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "SettingsViewController.h"
#import "CoreDataManager.h"
#import "Button.h"

@interface SettingsViewController ()
@property (strong, readwrite) UILabel* label;
@property (strong, readwrite) Button* signOutButton;
@end

@implementation SettingsViewController

- (id)init {
	self = [super init];
	if (self) {
		
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	CGSize labelSize = CGSizeMake(200, 50);
	CGSize boundsSize = self.view.bounds.size;
	self.label = [[UILabel alloc] initWithFrame:CGRectMake((boundsSize.width - labelSize.width) / 2,
													   200,
													   labelSize.width,
													   labelSize.height)];
	self.label.textColor = [UIColor blackColor];
	self.label.textAlignment = NSTextAlignmentCenter;
	self.label.lineBreakMode = NSLineBreakByTruncatingTail;
	[self.view addSubview:self.label];
	
	self.signOutButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[self.signOutButton setBackgroundImage:[UIImage imageNamed:@"button.png"] forState:UIControlStateNormal];
	self.signOutButton.imageEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 20);
	CGSize buttonSize = CGSizeMake(100, 44);
	self.signOutButton.frame = CGRectMake((boundsSize.width - buttonSize.width)/2,
									  CGRectGetMaxY(_label.frame) + 10,
									  buttonSize.width,
									  buttonSize.height);
	[self.signOutButton setTitle:@"Sign out" forState:UIControlStateNormal];
	[self.signOutButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[self.signOutButton setTitleShadowColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.signOutButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
	[self.signOutButton setTitleShadowColor:[UIColor blackColor] forState:UIControlStateHighlighted];
	self.signOutButton.titleLabel.shadowOffset = CGSizeMake(0, 1);
	[self.view addSubview:self.signOutButton];
	[self.signOutButton addTarget:self
								  action:@selector(didClickSignOutButton)
						forControlEvents:UIControlEventTouchUpInside];
	self.label.numberOfLines = 2;
	self.label.text = [NSString stringWithFormat:@"You are signed in as\n%@", self.user.username];
}

#pragma mark - Handlers

- (void)didClickSignOutButton {
	self.user.isCurrentUser = [NSNumber numberWithBool:NO];
	CoreDataManager* manager = [CoreDataManager sharedManager];
	[manager saveToDiskAsynchronously:YES];
	if (self.delegate && [self.delegate respondsToSelector:@selector(settingsViewController:didSignOutUser:)]) {
		[self.delegate settingsViewController:self didSignOutUser:self.user];
	}
}
@end
