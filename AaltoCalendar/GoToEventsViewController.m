//
//  GoToEventsViewController.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 26/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "GoToEventsViewController.h"
#import "Cell.h"
#import "CoreDataManager.h"
#import "Event.h"
#import "Config.h"
#import "Synchronizer.h"
#import "EventDetailViewController.h"

#define kCellIdentifier @"Cell"

@interface GoToEventsViewController ()
@property (strong, readwrite) NSFetchedResultsController* fetchController;
@end

@implementation GoToEventsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
		CoreDataManager* manager = [CoreDataManager sharedManager];
		NSFetchRequest* request = [[NSFetchRequest alloc] initWithEntityName:@"Event"];
		NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
		NSSortDescriptor* sortDescriptor2 = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
		request.sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, sortDescriptor2, nil];
		_fetchController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
															   managedObjectContext:manager.moc
																 sectionNameKeyPath:nil
																		  cacheName:nil];
		_fetchController.delegate = self;
		request.fetchBatchSize = 15;
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(synchronizerDidFinishSyncing)
													 name:DID_FINISH_SYNCHRONIZING object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(synchronizerDidFinishSyncing)
													 name:DID_FAIL_SYNCHRONIZING object:nil];
		self.title = @"GoTo";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	NSDate* fiveHoursAgo = [NSDate dateWithTimeIntervalSinceNow:(-5*3600)];
	NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(%K CONTAINS %@ || %K == %@) && %K > %@",
							  @"participants",
							  self.user,
							  @"owner",
							  self.user,
							  @"date",
							  fiveHoursAgo];
	self.fetchController.fetchRequest.predicate = predicate;
	UIRefreshControl* refreshControl = [[UIRefreshControl alloc] init];
	[refreshControl addTarget:[Synchronizer sharedSynchronizer] action:@selector(synchronize) forControlEvents:UIControlEventValueChanged];
	refreshControl.tintColor = [UIColor blackColor];
	self.refreshControl = refreshControl;
	NSError* error = nil;
	[self.fetchController performFetch:&error];
	if (error) {
		NSLog(@"couldn't fetch events:%@", error);
	}
	[self.tableView registerClass:Cell.class forCellReuseIdentifier:kCellIdentifier];
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.fetchController.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.fetchController.fetchedObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
	cell.event = [self.fetchController.fetchedObjects objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return kCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	EventDetailViewController* controller = [[EventDetailViewController alloc] init];
	controller.user = self.user;
	controller.event = [self.fetchController.fetchedObjects objectAtIndex:indexPath.row];
	[self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
	[self.tableView reloadData];
}

#pragma mark - Handlers

- (void)synchronizerDidFinishSyncing {
	[self.refreshControl endRefreshing];
}

@end
