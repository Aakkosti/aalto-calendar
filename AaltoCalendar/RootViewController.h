//
//  RootViewController.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 25/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SignUpViewController.h"
#import "SignInViewController.h"
#import "SettingsViewController.h"

@interface RootViewController : UIViewController <SignUpViewControllerDelegate, SignInViewControllerDelegate, UIAlertViewDelegate, SettingsViewControllerDelegate>
- (void)start;
- (void)showViewController:(UIViewController*)controller;
- (void)hideViewController:(UIViewController*)controller;
@end
