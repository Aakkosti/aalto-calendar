//
//  FileManager.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 2/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "FileManager.h"

static FileManager* sharedManager = nil;

@interface FileManager ()
@property (strong, readwrite) NSString* imagePath;
@end

@implementation FileManager

+ (FileManager*)sharedManager {
	if (!sharedManager) {
		sharedManager = [[FileManager alloc] init];
	}
	return sharedManager;
}

- (id)init {
	self = [super init];
	if (self) {
		_imagePath = [self imageDirectoryPath];
		_cache = [[NSCache alloc] init];
	}
	return self;
}

- (NSString*)imageDirectoryPath {
	NSArray* paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	NSString* cachePath = [paths objectAtIndex:0];
	NSString* imagesPath = [cachePath stringByAppendingString:@"/images"];
	NSFileManager* fileManager = [NSFileManager defaultManager];
	if (![fileManager fileExistsAtPath:imagesPath]) {
	    NSError *error;
		BOOL success = [[NSFileManager defaultManager] createDirectoryAtPath:imagesPath
												 withIntermediateDirectories:YES
																  attributes:nil
																	   error:&error];
		if (!success) {
			return nil;
		}
	}
	return imagesPath;
}

- (BOOL)writeDataToDisk:(NSData*)data withIdentifier:(NSString*)identifier {
	if (self.imagePath) {
		NSString* fullPath = [self.imagePath stringByAppendingString:identifier];
		return [data writeToFile:fullPath atomically:YES];
	}
	return NO;
}

- (NSData*)dataFromDiskWithIdentifier:(NSString *)identifier {
	NSString* fullPath = [self.imagePath stringByAppendingString:identifier];
	return [NSData dataWithContentsOfFile:fullPath];
}
@end
