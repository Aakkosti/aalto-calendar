//
//  AddressSearchViewController.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 2/05/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AddressSearchViewController;

@protocol AddressSearchViewControllerDelegate <NSObject>
- (void)addressController:(AddressSearchViewController*)controller
		 didSelectAddress:(NSString*)address
			 withLocation:(NSString*)location;
- (void)addressControllerDidClickCancel:(AddressSearchViewController *)controller;
@end

@interface AddressSearchViewController : UIViewController <UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDataSource, UITableViewDelegate>
@property (weak, readwrite) id delegate;
 @end
