//
//  MapViewController.m
//  AaltoCalendar
//
//  Created by Sébastien Villar on 30/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>

@interface MapViewController ()
@property (strong, readwrite) MKMapView* mapView;
@end

@implementation MapViewController

- (id)init {
	self = [super init];
	if (self) {
		
	}
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.mapView = [[MKMapView alloc] init];
	self.mapView.showsUserLocation = YES;
	[self.mapView addAnnotation:self.event];
	[self.view addSubview:self.mapView];
}

- (void)viewWillLayoutSubviews {
	self.mapView.frame = self.view.bounds;
	self.mapView.region = MKCoordinateRegionMake(self.event.coordinate, MKCoordinateSpanMake(0.01, 0.01));
}

@end
