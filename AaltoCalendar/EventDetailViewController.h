//
//  EventDetailViewController.h
//  AaltoCalendar
//
//  Created by Sébastien Villar on 29/04/13.
//  Copyright (c) 2013 Group 14. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import <MapKit/MapKit.h>
#import "User.h"
#import "AddEventViewController.h"

@interface EventDetailViewController : UIViewController <UIGestureRecognizerDelegate, MKMapViewDelegate, AddEventViewControllerDelegate>
@property (strong, readwrite) Event* event;
@property (strong, readwrite) User* user;
@end
